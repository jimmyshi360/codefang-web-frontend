/* eslint-disable react/jsx-props-no-spreading */
import React, { useState, useEffect } from 'react';
import { BrowserRouter as Router, Route } from 'react-router-dom';
import Navbar from './components/navbar';
import Home from './components/home';
import Login from './components/login';
import Team from './components/team';
import Problems from './components/problems';
import Profile from './components/profile';
import ProblemLoadScreen from './components/problem_load_screen';
import Admin from './components/admin';
import Signup from './components/signup';
import axios from 'axios';

function App() {
  const [loadScreen, setLoadScreen] = useState(false);
  const [loadedOnce, setLoadedOnce] = useState(false);
  const [navbarVisible, setNavbarVisible] = useState(true);
  const [isLoggedIn, setLoggedInStatus] = useState(false);
  const [name, setName] = useState('');
  const [email, setEmail] = useState('');
  const [userId, setUserId] = useState(-1);

  function getUsers() {
    axios('/api/auth/currentUser', {
      method: 'get',
      headers: {
        'Content-Type': 'text/plain',
      },
    }).then((response) => {
      setName(response.data.name);
      setEmail(response.data.email);
      setUserId(response.data.id);
      setLoggedInStatus(true);
    }).catch(error => {
       setLoggedInStatus(false);
    });
  }

  useEffect(()=>{
      getUsers();
  }
 , [isLoggedIn]);

  return (
    <div className="app">
      <Router>
        {
          navbarVisible
          && (
          <Navbar
            setNavbarVisible={setNavbarVisible}
            setLoadScreen={setLoadScreen}
            loadedOnce={loadedOnce}
            setLoadedOnce={setLoadedOnce}
            isLoggedIn={isLoggedIn}
          />
          )
        }
        
        <Route path="/home" render={(props) => <Home {...props} setNavbarVisible={setNavbarVisible} />} />
        <Route path="/team" render={(props) => <Team {...props} setNavbarVisible={setNavbarVisible}/>} />
        <Route path="/problems" render={(props) => <Problems {...props} 
          setNavbarVisible={setNavbarVisible}
          setLoadScreen={setLoadScreen}
          loadScreen={loadScreen}
          userId={userId}
          email={email}
          name={name}
          isLoggedIn={isLoggedIn}
        />} />
        {/*!isLoggedIn &&
        <Route path="/login" render={(props) => <Login {...props} setNavbarVisible={setNavbarVisible} setLoggedInStatus={setLoggedInStatus} setName={setName} setEmail={setEmail} setUserId={setUserId}/>} />
        */}
         <Route path="/login" render={(props) => <Login {...props} setNavbarVisible={setNavbarVisible} setLoggedInStatus={setLoggedInStatus} setName={setName} setEmail={setEmail} setUserId={setUserId}/>} />
        <Route path="/signup" render={(props) => <Signup {...props} setNavbarVisible={setNavbarVisible}/>} />
        
        {isLoggedIn &&
        <Route path="/profile" render={(props) => <Profile {...props} name={name} email={email}/>} />
        }
        <Route path="/admin" render={(props) => <Admin {...props} setNavbarVisible={setNavbarVisible} /> } />
      </Router>
    </div>
  );
}

export default App;
