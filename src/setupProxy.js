const proxy = require('http-proxy-middleware');

module.exports = function (app) {
  // TODO replace with configurable option
  app.use(proxy('/api/', { target: process.env.REACT_APP_BACKEND_URL+"/" }));
};
