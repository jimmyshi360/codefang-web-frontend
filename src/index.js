import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import './css/footer.css';
import './css/Home.css';
import './css/IDE.css';
import './css/monokai.css';
import './css/ayu-dark.css';
import './css/Navbar.css';
import './css/ProblemList.css';
import './css/Problems.css';
import './css/Profile.css';
import './css/Team.css';
import './css/Encyclopedia.css';
import './css/Output.css';
import './css/Submissions.css';
import './css/Syllabus.css';
import './css/Tutorial.css';
import "react-app-polyfill/ie11";
import "react-app-polyfill/stable";

import App from './App';
import * as serviceWorker from './serviceWorker';
import { transitions, positions, Provider as AlertProvider } from 'react-alert'
import AlertTemplate from './react-alert-template-basic'
import { faBorderNone } from '@fortawesome/free-solid-svg-icons';

const options = {
    // you can also just use 'bottom center'
    position: positions.BOTTOM_CENTER,
    timeout: 5000,
    offset: '15px',
    // you can also just use 'scale'
    transition: transitions.SCALE,
    type: 'info',
    containerStyle:{
        zIndex: 100,
    },
  }

const Root = () => (
    <AlertProvider template={AlertTemplate} {...options}>
      <App />
    </AlertProvider>
  )

ReactDOM.render(<Root />, document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
