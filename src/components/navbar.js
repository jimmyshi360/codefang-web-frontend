import React from 'react';
import { Link } from 'react-router-dom';
import Logo from '../imgs/logo.png';

function Navbar(props) {
  return (
    <nav className="navbar">
      <div className="nav-container">
      <a href="/home" style={{cursor: 'pointer'}}  onClick={() => props.setLoadScreen(false)}>
        <img className="logo-img" src={Logo}/>
      </a>
      <div className="link-container">
        <ul className="spec">
          
          <li>
            <Link className="link" to="/team" onClick={() => props.setLoadScreen(false)}>
              Team
            </Link>
          </li>
          <li>
            <Link
              className="link"
              to="/problems"
              onClick={() => {
                if (!props.loadedOnce) {
                  props.setLoadScreen(true);
                  props.setLoadedOnce(true);
                }
                props.setNavbarVisible(false);
              }}
            >
              Problems
            </Link>
          </li>
          <li style={{color: 'white'}}>
            |
          </li>
          <li>
            {!props.isLoggedIn &&
            <Link className="link" to="/login" onClick={() => props.setLoadScreen(false)}>
              Login
            </Link>
            }
          </li>
          <li>
          {props.isLoggedIn &&
            <Link className="link" to="/profile" onClick={() => props.setLoadScreen(false)}>
              Profile
            </Link>
          }
          </li>
        </ul>
        </div>
      </div>
    </nav>
  );
}

export default Navbar;
