/* eslint-disable react/prop-types */
/* eslint-disable react/prefer-stateless-function */
import React, {useEffect} from 'react';
import FeatureCards from './home/feature_cards';
import Slideshow from './home/slideshow';
import Testimonials from './home/testimonials';
import TopicSegments from './home/topic_segments';
import StickyFooters from './home/stickyFooters';
import ProgressionTreeSegment from './home/progression_tree_segment';
import TutorialSegment from './home/tutorial_segment';
import Pitch from './home/pitch';
import Companies from './home/companies';
import VerticalLine from './home/vertical_line';
import Curve4 from '../imgs/curve4.png';
import Wave from '../imgs/curve4.png';
import Particles from 'react-particles-js';

export default function Home(props) {
  useEffect(() => props.setNavbarVisible(true), []);

  return (
    <div className="home-container">
      <img src={Wave} className="background-curve"/>
      <Slideshow />
      {/*<Pitch/>*/}
      <Companies />
      <FeatureCards />
      <Testimonials />
      <TopicSegments />
      <ProgressionTreeSegment />
      <TutorialSegment />
      <VerticalLine />
      <StickyFooters />
    </div>
  );
}
