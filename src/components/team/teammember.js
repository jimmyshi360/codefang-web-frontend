/* eslint-disable react/jsx-one-expression-per-line */
/* eslint-disable react/destructuring-assignment */
/* eslint-disable react/prop-types */
import React from 'react';
import AOS from 'aos';
import 'aos/dist/aos.css';

function TeamMember(props) {
  AOS.init({ duration: 2000, once: true });
  return (
    <div
      className="teamMembers"
      data-aos="fade-up"
      data-aos-duration="1000"
      data-aos-delay={props.delay}
    >
      <img src={props.headshotURL} height={150} width={150} alt={props.name} />
      <h4>{props.name}- {props.position}</h4>
      <p className="companyTitle">{props.companyTitle}</p>
      <img className="greyscale" height={40} src={props.companyPicture} alt="company-logo" />
    </div>
  );
}

export default TeamMember;
