import React, { useState, useEffect } from 'react';
import '../css/Home.css';
import '../css/Login.css';
import GoogleButton from 'react-google-button';
import { Link } from 'react-router-dom';
import StickyFooters from './home/stickyFooters';
import axios from 'axios';

export default function Login(props) {


  return (
    <div className="login-background">
      <h1 className="login-welcome">
        Sign in to AlgoTree
      </h1>
      <div className="login-box">
        <img alt="CodeFang" src="" />
        <br />
        <form
          action="/api/auth/local"
          method="post">
          <div>
              <input
                className="login-text-box"
                placeholder="Email"
                type="text"
                name="email"
                onChange={(event) => {
                  props.setEmail(event.target.value);
                }}/>
          </div>
          <div>
              <input
                className="login-text-box"
                placeholder="Password"
                type="password"
                name="password"/>
          </div>
          <div>
              <input
                className="login-text-box"
                type="submit"
                value="Log In" />
          </div>
        </form>
        <br />
        <div style={{ textAlign: 'center' }}>
          <a
            onClick={() => props.setLoggedInStatus(true)}
            href="/api/auth/google"
            className="link-google">
            <GoogleButton />
          </a>
          <br />
          <a className="login-options" href="/api/auth/logout">
            Logout
          </a>
        </div>
        <Link to="/signup" className="login-options">
          New user? Sign up here.
        </Link>
      </div>
      <br />
      <br />
      <StickyFooters />
    </div>
  );
}

/*
 *
 <div>
            <GoogleLogin
              clientId="658977310896-knrl3gka66fldh83dao2rhgbblmd4un9.apps.googleusercontent.com"
              buttonText="Login with Google"
              onSuccess={responseGoogle}
              onFailure={responseGoogle}
              cookiePolicy="single_host_origin"
            />
          </div>
  */
