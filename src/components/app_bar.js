import React, { useState } from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faAtlas, faCodeBranch, faCode, faList, faExpandAlt, faCompress, faUsers } from '@fortawesome/free-solid-svg-icons'
import ReactSearchBox from 'react-search-box';
import Logo from '../imgs/logowhite2.png';
//import Logo from '../imgs/hedge.png';
import Autosuggest from 'react-autosuggest';
import theme from '../css/SearchBar.css';
import Toggle from 'react-toggle';
import "react-toggle/style.css"
import ProfilePic from '../imgs/profile.png';

export default function AppBar(props) {

  const data = [
    {
      key: 'Iterative Binary Search',
      value: 'Iterative Binary Search',
    },
    {
      key: 'Recursive Binary Search',
      value: 'Recursive Binary Search',
    },
  ]


  const [searchValue, setSearchValue] = useState('');
  const [suggestions, setSuggestions] = useState([]);
  const [profileClicked, setProfileClicked] = useState(false);

  const problems = [
    {
      name: 'Iterative Binary Search',
      id: 1
    },
    {
      name: 'Recursive Binary Search',
      id: 2
    },
    {
      name: 'Binary Search Insertion',
      id: 3
    },
  ];

  const getSuggestions = value => {
    const inputValue = value.trim().toLowerCase();
    const inputLength = inputValue.length;

    return inputLength === 0 ? [] : problems.filter(p =>
      p.name.toLowerCase().slice(0, inputLength) === inputValue
    );
  };

  const getSuggestionValue = suggestion => suggestion.name;

  const renderSuggestion = suggestion => (
    <div>
      {suggestion.name}
    </div>
  );

  const onChange = (event, { newValue }) => {
    setSearchValue(newValue);
  };

  // Autosuggest will call this function every time you need to update suggestions.
  // You already implemented this logic above, so just use it.
  const onSuggestionsFetchRequested = ({ value }) => {
    setSuggestions(getSuggestions(value));
  };

  // Autosuggest will call this function every time you need to clear suggestions.
  const onSuggestionsClearRequested = () => {
    setSuggestions([]);
  };

  const inputProps = {
    placeholder: 'Type a problem name',
    value: searchValue,
    onChange: onChange
  };

  return (
    <nav className="top-navbar">
      <div className="top-container">
        <div className="condensed-logo-container">
          <a href="/home" style={{ marginLeft: 'auto' }}><img src={Logo} className="condensed-logo" /></a>
        </div>
        <div>
          <ul className="spec2" style={{ marginTop: '0px', paddingLeft: '0', display: 'flex' }}>

            {/*<li className={props.problemIndex == 0 ? "accordian-li selected-accordian" : "accordian-li"}>
              <a className={props.problemIndex == 0 ? "glowing condensed-link" : 'condensed-link'} onClick={() => props.setProblemIndex(0)}>
                <FontAwesomeIcon icon={faAtlas} className={props.problemIndex == 0 ? "glowing condensed-icon" : "condensed-icon"} /> Encyclopedia
                
              </a>
            </li>*/}
            
            <li className={props.problemIndex != 3 ? "accordian-li-b selected-accordian" : "accordian-li-b"}>
              <a className={props.problemIndex != 3 ? "glowing condensed-link-b" : 'condensed-link-b'} onClick={() => {props.setProblemIndex(1); }}>
                <FontAwesomeIcon icon={faCodeBranch} className={props.problemIndex != 3 ? "glowing condensed-icon" : "condensed-icon"} />  &nbsp; Skill Tree
              </a>
            </li>
            <li className={props.problemIndex == 3 ? "accordian-li-b selected-accordian" : "accordian-li-b"}>
              <a className={props.problemIndex == 3 ? "glowing condensed-link-b" : 'condensed-link-b'} onClick={() => props.setProblemIndex(3)}>
                <FontAwesomeIcon icon={faList} className={props.problemIndex == 3 ? "glowing condensed-icon" : "condensed-icon"} />  &nbsp; Problem List
              </a>
            </li>
            <li>
            <ReactSearchBox
              placeholder="Search for a Problem"
              value=""
              data={data}
              callback={record => console.log(record)}
              inputBoxHeight={'30px'}
              style={{borderRadius: '100% important'}}
            />
            </li>
      {/*
            <li>
              <Autosuggest
                suggestions={suggestions}
                onSuggestionsFetchRequested={onSuggestionsFetchRequested}
                onSuggestionsClearRequested={onSuggestionsClearRequested}
                getSuggestionValue={getSuggestionValue}
                renderSuggestion={renderSuggestion}
                inputProps={inputProps}
                theme={theme}
              />
            </li>

            <li className="accordian-li">
              <span className="condensed-link">Night Mode</span>
              <Toggle
                defaultChecked={true}
                onChange={() => { }} />
            </li>
      */}
       {/* <li className={props.problemIndex == 4 ? "accordian-li selected-accordian" : "accordian-li"}>
              <a className={props.problemIndex == 4 ? "glowing condensed-link" : 'condensed-link'} onClick={() => {props.setProblemIndex(4); }}>
                <FontAwesomeIcon icon={faUsers} className={props.problemIndex == 4 ? "glowing condensed-icon" : "condensed-icon"} />
              </a>
            </li>*/}
          </ul>

          {/*
        </div>
        <div style={{right: '0', position: 'absolute'}}>
          <img onClick={()=>setProfileClicked(!profileClicked)} style={{width: '30px', cursor: 'pointer', height: '30px', margin: '5px', marginRight: '20px'}} src={ProfilePic}></img>
        </div>
          */}
               
               </div>
        <div style={{position: 'absolute', right: '35px', top: '3px'}}>
        <a className="fullscreen-icon" onClick={() => { document.fullscreenElement ? document.exitFullscreen() : document.body.requestFullscreen() }} >
                <FontAwesomeIcon className="condensed-icon" icon={document.fullscreenElement ? faCompress : faExpandAlt} />
              </a>
        </div>


        {profileClicked && 
        
        (<div style={{right: '35px', top: '35px',  position: 'absolute', height: '200px'}}>
            <div style={{backgroundColor: 'white', width: '300px'}}>
              {props.name}
              <br/>
              {props.email}
              
            </div>
          </div>
        )}


      </div>
    </nav>
  );
}
