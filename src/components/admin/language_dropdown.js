import React from 'react';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';

export default function LanguageDropdown(props) {

  return (
    <FormControl style={{ margin: '15px' }}>
      <InputLabel >Language</InputLabel>
      <Select
        value={props.lang}
        onChange={(e) => props.setLang(e.target.value)}
        style={{ width: '150px' }}
      >
        <MenuItem style={{ width: '150px' }} value={'java'}>Java</MenuItem>
        <MenuItem style={{ width: '150px' }} value={'python'}>Python 3</MenuItem>
        <MenuItem style={{ width: '150px' }} value={'cpp'}>C++</MenuItem>
        <MenuItem style={{ width: '150px' }} value={'js'}>JavaScript</MenuItem>
      </Select>
    </FormControl>
  );
}