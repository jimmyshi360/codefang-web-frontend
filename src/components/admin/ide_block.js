import React from 'react';
import { Controlled as CodeMirror } from 'react-codemirror2';
import 'react-sigma/sigma/main';
import 'react-sigma/sigma/parsers.json';
import 'codemirror/lib/codemirror.css';
import 'codemirror/mode/javascript/javascript';
import 'codemirror/mode/clike/clike';
import 'codemirror/mode/python/python';
import 'codemirror/mode/xml/xml';
import 'codemirror/mode/markdown/markdown';
import 'codemirror/addon/edit/closebrackets';

export default function IDEBlock(props) {
  const codemirrorLang = {
    js: "application/javascript",
    cpp: "text/x-c++src",
    java: "text/x-java",
    python: "text/x-python"
  }
  return (
    <CodeMirror
      value={props.code}
      className="admin-ide"
      onBeforeChange={(editor, data, value) => {
        props.setCode(value);
      }}
      options={{
        lineNumbers: true,
        mode: codemirrorLang[props.lang],
        lineWrapping: true,
        theme: 'ayu-dark',
        tabMode: 'indent',
        autoMatchParens: true,
        smartIndent: true,
        tabSize: 4,
        indentUnit: 4,
        matchBrackets: true,
        autoCloseBrackets: true,
      }}
    />
  );
}