import React, { useState } from 'react';
import Typography from '@material-ui/core/Typography';
import TextField from '@material-ui/core/TextField';
import DialogTitle from '@material-ui/core/DialogTitle';
import Dialog from '@material-ui/core/Dialog';
import LanguageDropdown from './language_dropdown';
import IDEBlock from './ide_block';
import axios from 'axios';
import Button from '@material-ui/core/Button';
import 'react-sigma/sigma/main';
import 'react-sigma/sigma/parsers.json';
import 'codemirror/lib/codemirror.css';
import 'codemirror/mode/javascript/javascript';
import 'codemirror/mode/clike/clike';
import 'codemirror/mode/python/python';
import 'codemirror/mode/xml/xml';
import 'codemirror/mode/markdown/markdown';
import 'codemirror/addon/edit/closebrackets';


export default function CreateProblemModal(props) {
  const [createProblemTitle, setCreateProblemTitle] = useState('Title');
  const [createProblemDescription, setCreateProblemDescription] = useState('Description');
  const [createProblemSolution, setCreateProblemSolution] = useState('Solution');

  const [lang, setLang] = useState('java');

  const [createJAVATemplateCode, setCreateJAVATemplateCode] = useState('// Code\nclass Example {\n\tpublic int[] problemTitle(int param1, int[] param2) {\n\n\n\t}\n}');
  const [createPYTHONTemplateCode, setCreatePYTHONTemplateCode] = useState('// Code\nclass Solution:\n\tdef twoSum(self, nums: List[int], target: int) -> List[int]:\n\n\n');
  const [createCPPTemplateCode, setCreateCPPTemplateCode] = useState('// Code\nclass Solution {\npublic:\n\tvector<int> problemTitle(vector<int>& nums, int target) {\n\n\n\t}\n};');
  const [createJSTemplateCode, setCreateJSTemplateCode] = useState('// Code\nvar problemTitle = function(param1, param2) {\n\n\n\n};');

  const [createJAVATestCode, setCreateJAVATestCode] = useState('// Code\nclass Example {\n\tpublic int[] problemTitle(int param1, int[] param2) {\n\n\n\t}\n}');
  const [createPYTHONTestCode, setCreatePYTHONTestCode] = useState('// Code\nclass Solution:\n\tdef twoSum(self, nums: List[int], target: int) -> List[int]:\n\n\n');
  const [createCPPTestCode, setCreateCPPTestCode] = useState('// Code\nclass Solution {\npublic:\n\tvector<int> problemTitle(vector<int>& nums, int target) {\n\n\n\t}\n};');
  const [createJSTestCode, setCreateJSTestCode] = useState('// Code\nvar problemTitle = function(param1, param2) {\n\n\n\n};');

  const [dataFile, setDataFile] = useState('//test data');

  function updateAlert(isVisible, isError, text) {
    props.setAlertText(text+"");
    props.setIsError(isError);
    props.setAlertOpen(isVisible);
  }

  function postProblem() {
    let id = -1;
    axios(process.env.REACT_APP_BACKEND_URL + '/api/problems', {
      method: 'post',
      headers: {
        'Content-Type': 'text/plain',
      },
      params: {
        title: createProblemTitle,
        text: createProblemDescription,
        adminPass: global.adminPassword
      },
    }).then((response) => {
      const createdArray = response.data.split(' ');
      id = createdArray[createdArray.length - 1];
      updateAlert(true, false, "Problem created");
      props.setCreateProblemModalVisible(false);
      props.getProblems();

      // Subsequently update templates if problem was posted
      axios(process.env.REACT_APP_BACKEND_URL + '/api/templates', {
        method: 'post',
        headers: {
          'Content-Type': 'text/plain',
        },
        params: {
          problem_id: id,
          java: createJAVATemplateCode,
          python3: createPYTHONTemplateCode,
          cpp: createCPPTemplateCode,
          javascript: createJSTemplateCode,
          adminPass: global.adminPassword
        },
      }).then((response) => {
        console.log(response);
      }).catch((error) => {
        console.log(error);
      });

      axios(process.env.REACT_APP_BACKEND_URL + '/api/kubernetes/settest', {
        method: 'post',
        headers: {
          'Content-Type': 'application/json',
        },
        params: {
          problemId: id,
          problemTopic: props.currTopic.title.replace(/\s/g, ''),
          problemName: createProblemTitle.replace(/\s/g, ''),
          dataFile: dataFile,
          adminPass: global.adminPassword
        },
        data: {
          java: createJAVATestCode,
          python: createPYTHONTestCode,
          javascript: createJSTestCode,
          cpp: createCPPTestCode
        }
      }).then((response) => {
        console.log(response);
      }).catch((error) => {
        console.log(error);
      });
    }).catch((error) => {
      updateAlert(true, true, error);
    });
  }

  function filterLanguageTemplate() {
    switch (lang) {
      case 'python':
        return createPYTHONTemplateCode;
        break;
      case 'cpp':
        return createCPPTemplateCode;
        break;
      case 'java':
        return createJAVATemplateCode;
        break;
      case 'js':
        return createJSTemplateCode;
        break;
    }
  }

  function filterLanguageSetterTemplate() {
    switch (lang) {
      case 'python':
        return setCreatePYTHONTemplateCode;
        break;
      case 'cpp':
        return setCreateCPPTemplateCode;
        break;
      case 'java':
        return setCreateJAVATemplateCode;
        break;
      case 'js':
        return setCreateJSTemplateCode;
        break;
    }
  }


  function filterLanguageTest() {
    switch (lang) {
      case 'python':
        return createPYTHONTestCode;
        break;
      case 'cpp':
        return createCPPTestCode;
        break;
      case 'java':
        return createJAVATestCode;
        break;
      case 'js':
        return createJSTestCode;
        break;
    }
  }

  function filterLanguageSetterTest() {
    switch (lang) {
      case 'python':
        return setCreatePYTHONTestCode;
        break;
      case 'cpp':
        return setCreateCPPTestCode;
        break;
      case 'java':
        return setCreateJAVATestCode;
        break;
      case 'js':
        return setCreateJSTestCode;
        break;
    }
  }

  return (

    <Dialog maxWidth='xl' onClose={() => props.setCreateProblemModalVisible(false)} open={props.createProblemModalVisible}>
      <DialogTitle>Create Problem</DialogTitle>
      <TextField style={{ margin: '15px', width: '350px' }} id="outlined-basic" label="Problem Title" value={createProblemTitle} onChange={(e) => setCreateProblemTitle(e.target.value)} />

      <TextField
        id="outlined-multiline-static"
        label="Problem Description"
        multiline
        rows="4"
        style={{ width: '80vw', margin: '15px' }}
        defaultValue=""
        variant="outlined"
        value={createProblemDescription}
        onChange={(e) => setCreateProblemDescription(e.target.value)}
      />

      <TextField
        id="outlined-multiline-static"
        label="Problem Solution"
        multiline
        rows="4"
        style={{ width: '80vw', margin: '15px' }}
        defaultValue=""
        variant="outlined"
        value={createProblemSolution}
        onChange={(e) => setCreateProblemSolution(e.target.value)}
      />

      <LanguageDropdown setLang={setLang} lang={lang} />
      <div className="admin-ide-container">
        <div className="admin-ide-block">
          <div className="admin-ide-topbar">
            <Typography style={{ margin: '15px' }} variant="h5">Template Code</Typography>
          </div>
          <IDEBlock setCode={filterLanguageSetterTemplate()} code={filterLanguageTemplate()} lang={lang} />
        </div>
        <div className="admin-ide-block">
          <div className="admin-ide-topbar">
            <Typography style={{ margin: '15px' }} variant="h5">Test Case Code</Typography>
          </div>
          <IDEBlock setCode={filterLanguageSetterTest()} code={filterLanguageTest()} lang={lang} />
        </div>
      </div>

      <TextField
        id="outlined-multiline-static"
        label="Data File"
        multiline
        rows="4"
        style={{ width: '80vw', margin: '15px' }}
        defaultValue=""
        variant="outlined"
        value={dataFile}
        onChange={(e) => setDataFile(e.target.value)}
      />
      <Button style={{ margin: '15px' }} variant="contained" onClick={postProblem} color="secondary">
        Post Problem
      </Button>
    </Dialog>
  );
}
