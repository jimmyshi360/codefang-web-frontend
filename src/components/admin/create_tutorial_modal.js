import React, { useState } from 'react';
import Typography from '@material-ui/core/Typography';
import TextField from '@material-ui/core/TextField';
import DialogTitle from '@material-ui/core/DialogTitle';
import Dialog from '@material-ui/core/Dialog';
import LanguageDropdown from './language_dropdown';
import IDEBlock from './ide_block';
import axios from 'axios';
import Button from '@material-ui/core/Button';
import 'react-sigma/sigma/main';
import 'react-sigma/sigma/parsers.json';
import 'codemirror/lib/codemirror.css';
import 'codemirror/mode/javascript/javascript';
import 'codemirror/mode/clike/clike';
import 'codemirror/mode/python/python';
import 'codemirror/mode/xml/xml';
import 'codemirror/mode/markdown/markdown';
import 'codemirror/addon/edit/closebrackets';

export default function CreateTutorialModal(props) {
  const [createTutorialTitle, setCreateTutorialTitle] = useState('Title');
  const [createTutorialText, setCreateTutorialText] = useState('Text');

  function updateAlert(isVisible, isError, text) {
    props.setAlertText(text+"");
    props.setIsError(isError);
    props.setAlertOpen(isVisible);
  }

  function postTutorial() {
    axios(process.env.REACT_APP_BACKEND_URL + '/api/tutorials', {
      method: 'post',
      headers: {
        'Content-Type': 'application/json',
      },
      params: {
        title: createTutorialTitle,
        adminPass: global.adminPassword
      },
      data: {
        text: createTutorialText,
      },
    }).then((response) => {
      console.log(response);
      updateAlert(true, false, "Tutorial created");
      props.setCreateTutorialModalVisible(false);
      props.getTutorials();
      });
  }

  return (
    <Dialog maxWidth='xl' onClose={() => props.setCreateTutorialModalVisible(false)} open={props.createTutorialModalVisible}>
      <DialogTitle>Create Tutorial</DialogTitle>
      <TextField style={{ margin: '15px', width: '350px' }} id="outlined-basic" label="Tutorial Title" value={createTutorialTitle} onChange={(e) => setCreateTutorialTitle(e.target.value)} />

      <TextField
        id="outlined-multiline-static"
        label="Tutorial Text"
        multiline
        rows="4"
        style={{ width: '80vw', margin: '15px' }}
        defaultValue=""
        variant="outlined"
        value={createTutorialText}
        onChange={(e) => setCreateTutorialText(e.target.value)}
      />
      <Button style={{ margin: '15px' }} variant="contained" onClick={postTutorial} color="secondary">
        Post Tutorials
      </Button>
    </Dialog>
  );
}
