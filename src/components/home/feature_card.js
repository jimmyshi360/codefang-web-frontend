import React from 'react';
import '../../css/Home.css';

function FeatureCard() {
    return (
        <div className="feature-card-container">
            <img src="" alt="ICON" />
            <div className="feature-title">
                250+ Hand Picked Problems
            </div>
            <p className="feature-description">
                This is a whole long description about the problems and why theyre so great blah blah blah yada yada yada etc etc etc efkjqbefliu132bg5oubfhybgtu3nflkg4j2boi3 v2erhv 2oiurgb 2i34pugnfiuprw f4ghu v13hf
            </p>
        </div>
    );
}

export default FeatureCard;
