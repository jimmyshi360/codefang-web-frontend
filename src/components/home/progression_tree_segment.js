import React from 'react';
import 'aos/dist/aos.css';

function ProgressionTreeSegment() {
    return (
        <div className="segment-container">
            <div className="left-explain-panel"> 
                <div className="title"> Track your Learning Journey. </div>
                <div className="description"> We walk you through everything from syntax basics to data structures to algorithms, as you level up and track your progress in various skill areas through our skill trees. As you journey through the mystical land of all that is computer science, you will be able to earn badges and tokens for your profile, etc etc etc blah blah blah. </div>
            </div>
            <div className="example-graph-holder">
                <img src="../../../graph.png" alt="GRAPH" />
            </div>
        </div>
    );
}

export default ProgressionTreeSegment;