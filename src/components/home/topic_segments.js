import React from 'react';
import AOS from 'aos';
import 'aos/dist/aos.css';

function TopicSegments() {
  AOS.init({ duration: 2000, once: true });
  return (
    <div className="topic-segments-container">
      <div className="segment-title"> 250+ Questions in 15 Categories </div>
      <div className="topic-segments-description"> If you want to ace the tech interview, being well-versed in all common data structures and popular problem-solving methods is paramount. With 85 questions spanning 14 categories and 5 difficulty levels, we've got you covered. </div>
      <div className="topics-container">
        <div
          className="topics-item"
          data-aos="fade-up"
          data-aos-duration="750"
          data-aos-delay="0"
        >
          {' '}
Strings
        </div>
        <div
          className="topics-item"
          data-aos="fade-up"
          data-aos-duration="750"
          data-aos-delay="100"
        >
          {' '}
Arrays
        </div>
        <div
          className="topics-item"
          data-aos="fade-up"
          data-aos-duration="750"
          data-aos-delay="200"
        >
          {' '}
Trees
        </div>
        <div
          className="topics-item"
          data-aos="fade-up"
          data-aos-duration="750"
          data-aos-delay="300"
        >
          {' '}
Heaps
        </div>
        <div
          className="topics-item"
          data-aos="fade-up"
          data-aos-duration="750"
          data-aos-delay="400"
        >
          {' '}
Sorts
        </div>
        <div
          className="topics-item"
          data-aos="fade-up"
          data-aos-duration="750"
          data-aos-delay="500"
        >
          {' '}
Binary Search
        </div>
        <div
          className="topics-item"
          data-aos="fade-up"
          data-aos-duration="750"
          data-aos-delay="600"
        >
          {' '}
Graphs
        </div>
        <div
          className="topics-item"
          data-aos="fade-up"
          data-aos-duration="750"
          data-aos-delay="700"
        >
          {' '}
Searches
        </div>
        <div
          className="topics-item"
          data-aos="fade-up"
          data-aos-duration="750"
          data-aos-delay="800"
        >
          {' '}
Product Management
        </div>
        <div
          className="topics-item"
          data-aos="fade-up"
          data-aos-duration="750"
          data-aos-delay="900"
        >
          {' '}
System Design
        </div>
      </div>
    </div>
  );
}

export default TopicSegments;
