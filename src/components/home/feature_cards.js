import React from 'react';

function FeatureCards() {
  return (
    <div className="about-block">
      <h1>What do We Offer?</h1>
      <div className="flex-container">
        <div className="flex-item">250+ Hand Picked Problems</div>
        <div className="flex-item">Carefully Ordered Progression Trees that Teach Skills in Order</div>
        <div className="flex-item">Detailed interactive tutorial segments</div>
        <div className="flex-item">Built in development environment</div>
        <div className="flex-item">Big O efficiency analysis</div>
        <div className="flex-item">Solutions and problems in Java, Python, Javascript and C#</div>
      </div>
    </div>
  );
}

export default FeatureCards;
