import React from 'react';
import AOS from 'aos';
import 'aos/dist/aos.css';
import DChu from '../../imgs/dChu.jpeg';
import JYu from '../../imgs/jYu.jpeg';
import RSam from '../../imgs/rSam.jpeg';
import gLogo from '../../imgs/companies/google.png';
import fbLogo from '../../imgs/companies/facebook.png';
import nasaLogo from '../../imgs/nasaLogo.png';
import TestimonialPeople from "./testimonialpeople";

function Testimonials(props) {
    const testimonies = [{
        headshotURL: DChu,
        name: 'Daniel Chu',
        companyTitle: 'Incoming Software Engineering Intern',
        companyPicture: gLogo,
        quote: "I felt that this service was giving me a much better sense\n" +
            "of progression than other services on the market.",
    }, {
        headshotURL: JYu,
        name: 'Jack Yu',
        companyTitle: 'Incoming Software Engineering Intern',
        companyPicture: fbLogo,
        quote: "I went from being an aspiring premed student to a software" +
            " engineer at Facebook within a couple weeks thanks to grinding" +
            " it out on CodeFang.",
    }, {
        headshotURL: RSam,
        name: 'Raghav Sambasivan',
        companyTitle: 'Incoming Software Engineering Intern',
        companyPicture: nasaLogo,
        quote: "Nasa is cool. I like space.",
    }, {
        headshotURL: RSam,
        name: 'Raghav Sambasivan',
        companyTitle: 'Incoming Software Engineering Intern',
        companyPicture: nasaLogo,
        quote: "Nasa is cool. I like space.",
    }, {
        headshotURL: RSam,
        name: 'Raghav Sambasivan',
        companyTitle: 'Incoming Software Engineering Intern',
        companyPicture: nasaLogo,
        quote: "Nasa is cool. I like space.",
    }];
  AOS.init({ duration: 2000, once: true });
  return (
      <div className="testimonials-container">
          {testimonies.map((item) => (
              <TestimonialPeople
              headshotURL = {item.headshotURL}
              name={item.name}
              companyTitle={item.companyTitle}
              companyPicture={item.companyPicture}
              quote={item.quote}
              />
              ))}
      </div>
  );
}

export default Testimonials;
