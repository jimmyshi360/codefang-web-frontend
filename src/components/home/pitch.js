import React from 'react';

function Pitch() {
  return (
    <div className="pitch-container">
      <p>
        Tired of LeetCode?
      </p>
      <p>
        Looking for a guided course with skill trees and hand curated questions by National level programmers and FANG engineers?
      </p>
      <p>
        Go Zero to Hero with CodeFang! Intended for beginners and advanced coders!
      </p>

      <button href="/problems">Get started!</button>
    </div>
  );
}

export default Pitch;
