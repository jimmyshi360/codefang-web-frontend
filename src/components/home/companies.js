import React from 'react';
import Google from '../../imgs/companies/google.png';
import Facebook from '../../imgs/companies/facebook.png';
import NASA from '../../imgs/companies/nasa.png';
import Amazon from '../../imgs/companies/amazon.png';
import Qualtrics from '../../imgs/companies/qualtrics.svg';
import Palantir from '../../imgs/companies/palantir.png';
import PayPal from '../../imgs/companies/paypal.png';
import Bloomberg from '../../imgs/companies/bloomberg.png';
import MongoDB from '../../imgs/companies/mongodb.png';

export default function Companies() {
  return (
    <>
      <p  style={{ textAlign: 'center', marginBottom: '5px', marginTop: '55px' }}>Users have gotten offers from:</p>
      <div className="companies-container">
        <div id="crossfade">
          <img src={Amazon} alt="Amazon" />
          <img src={Facebook} alt="Facebook" />
          <img src={Google} alt="Google" />
          <img src={Palantir} alt="Palantir" />
          <img src={PayPal} alt="PayPal" />
          <img src={Bloomberg} alt="Bloomberg" />
          <img src={NASA} alt="NASA" />
          <img src={Qualtrics} alt="Qualtrics" />
          <img src={MongoDB} alt="MongoDB" />
        </div>
      </div>
    </>
  );
}
