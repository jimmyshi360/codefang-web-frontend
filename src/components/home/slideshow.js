import React, { useState } from 'react';
import Slider from 'react-slick';
import 'slick-carousel/slick/slick.css';
import 'slick-carousel/slick/slick-theme.css';
import HomeVid from '../../imgs/home.mp4';

export default function Slideshow() {
  const [settings, setSettings] = useState({
    autoplay: false,
    autoplaySpeed: 9000,
    dots: true,
    infinite: true,
    speed: 500,
    slidesToShow: 1,
    slidesToScroll: 1,
    arrows: false,
  });

  return (
    <div className="slideshow">
      <Slider {...settings} >
        <div className="slideshow-component">
          <div style={{width: '100%', maxWidth: '1500px', display: 'flex', margin: 'auto'}}>
            <div style={{margin: 'auto', textAlign: 'left'}}>
              <h2 className="hero-title" style={{color:'red'}}>Under Construction. Check back in August 2020.</h2>
              <br></br>
              <h2 className="hero-title" >Looking for a comprehensive guide to technical interviews?</h2>
              <h3 className="hero-subtitle">Introducing an all-in-one learning platform for technical interviews</h3>
              <h2 className="hero-subtitle">Helping beginners and experienced coders land their dream tech jobs!</h2>
              <br></br>
              <button className="hero-button" href="/problems">Level Up Now</button>
            </div>
            <div>
              <video className="background-video" loop autoPlay>
                <source src={HomeVid} type="video/mp4" />
                <source src={HomeVid} type="video/ogg" />
                Your browser does not support the video tag.
              </video>
              <br></br>
              <h3 className="hero-subtitle">Skill Trees · Curated Questions by National Competitive Coders · Sleek IDE · Job App Spreadsheet</h3> 
            </div>
          </div>
        </div>
        
        <div className="slideshow-component">
          <h3>2</h3>
        </div>
        <div className="slideshow-component">
          <h3>3</h3>
        </div>
        <div className="slideshow-component">
          <h3>4</h3>
        </div>
        <div className="slideshow-component">
          <h3>5</h3>
        </div>
        <div className="slideshow-component">
          <h3>6</h3>
        </div>
      </Slider>
    </div>
  );
}
