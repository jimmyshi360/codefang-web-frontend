import React from 'react';
import StickyFooter from 'react-sticky-footer';
import Fbwhite from '../../imgs/fb-white.png';
import 'aos/dist/aos.css';

function StickyFooters(props) {
  return (
    <StickyFooter
      bottomThreshold={50}
    >
      <div className="logo"><text>COMPANY NAME LOGO HERE</text></div>

      <div id="wrapper">
        <div class="grid-container grid-container--fit">
          <div class="grid-element">
            <h4>Home</h4>
            <a href="/home">Home1</a><br></br>
            <a href="/home">Home2</a><br></br>
          </div>
          <div class="grid-element">
            <h4>Team</h4>
            <a href="/home">Our Vision</a><br></br>
            <a href="/home">Our Tech Platform</a><br></br>
            <a href="/home">Our Service Model</a><br></br>
          </div>
          <div class="grid-element">
            <h4>Problems</h4>
            <a href="/home">Amazon Questions</a><br></br>
            <a href="/home">Facebook Questions</a><br></br>
            <a href="/home">Google Questions</a><br></br>
          </div>
          <div class="grid-element">
            <h4>Login</h4>
            <a href="/home">Here's a link</a><br></br>
            <a href="/home">Here's another link</a><br></br>
          </div>
          <div class="grid-element">
            <h4>Profile</h4>
            <a href="/home">Blog</a><br></br>
          </div>
        </div>

        <div className="copyright">
          <a href="https://www.facebook.com/"><img src={Fbwhite} alt="Fbwhite" height="25" width="25" style={{marginRight: '10px'}}/></a>
          <a href="https://www.facebook.com/"><img src={Fbwhite} alt="Fbwhite" height="25" width="25" style={{marginRight: '10px'}}/></a>
          <a href="https://www.facebook.com/"><img src={Fbwhite} alt="Fbwhite" height="25" width="25" style={{marginRight: '10px'}}/></a>
          <p>Copyright © 2020 CodeFang</p>
          <p><a href="/home">Privacy Policy</a></p>
          <p><a href="/home">Terms of Use</a></p>
        </div>


      </div>




    </StickyFooter>
  );
}

export default StickyFooters;
