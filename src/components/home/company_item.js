import React from 'react';

export default function CompanyItem(props) {

  return (
    <div className="company-item">
      <img className="company-item-img" src={props.companyImage} alt="company"/>
    </div>
  );
}
