import React from 'react';
import AOS from 'aos';
import 'aos/dist/aos.css';

function TestimonialPeople(props) {
    AOS.init({ duration: 2000, once: true });
    return (
        <div
            className="testimonials"
            data-aos="fade-up"
            data-aos-duration="1000"
            data-aos-delay={props.delay}
        >
            <img className="circlePics" height={250} width={250} src={props.headshotURL}  alt={props.name} />
            <h4 className="testimonial-name">{props.name}</h4>
            <p className="companyTitle">{props.companyTitle}</p>
            <img className="testimonial-company" height={100} src={props.companyPicture} alt="company-logo" />
            <p className="testimonial-quote">{props.quote}</p>
        </div>
    );
}

export default TestimonialPeople;
