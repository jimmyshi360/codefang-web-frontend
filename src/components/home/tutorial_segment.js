import React from 'react';
import AOS from 'aos';
import 'aos/dist/aos.css';

function TutorialSegment() {
    return (
        <div className="segment-container">
            <div className="example-graph-holder">
                <img src="../../../graph.png" alt="VIDEO" />
            </div>
            <div className="left-explain-panel"> 
                <div className="title"> Interactive Tutorials </div>
                <div className="description"> 
                    The truth is that interview questions and algorithms are difficult. Even more difficult is trying to understand the inner workings of a complex algorithm from a book or from a poorly thought-out video shot on your grandma's camera. Our crisp 1080p videos boast crystal-clear audio and are strategically divided into two parts to give you the most comprehensive explanations to questions. That's over 55 hours of content specifically tailored to make interview questions and algorithms easy.
                </div>
            </div>
        </div>
    );
}

export default TutorialSegment;