import React, { PureComponent } from "react";
import PropTypes from "prop-types";
import 'codemirror/addon/runmode/runmode'
import 'codemirror/mode/meta'
import { Controlled as CodeMirror } from 'react-codemirror2';
import 'codemirror/lib/codemirror.css';
import 'codemirror/mode/clike/clike';
require('../../autorefresh.ext');

class CodeBlock extends PureComponent {
  static propTypes = {
    value: PropTypes.string.isRequired,
    language: PropTypes.string
  };

  static defaultProps = {
    language: null
  };

  render() {
    const { language, value } = this.props;
    return (
      <CodeMirror
          value={value}
          className="code-block-mirror"
          options={{
            lineNumbers: true,
            mode: 'text/x-java',
            lineWrapping: true,
            theme: 'monokai',
            tabMode: 'indent',
            autoMatchParens: true,
            smartIndent: true,
            tabSize: 4,
            indentUnit: 4,
            matchBrackets: true,
            autoCloseBrackets: true,
            autoRefresh: {force: true}
          }}
        />
    );
  }
}

export default CodeBlock;