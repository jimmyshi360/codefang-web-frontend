import React, { useState, useEffect } from 'react';
import Speedometer from './temp-speedometer.png'
function TestCaseDetails(props) {
    
    return (
        <>
        {props.output!=undefined && 
        <div className="testcase-details">
            <h3 className="testcase-noncode">Compiler Message</h3>
            <h3 className="testcase-code">{props.output.data.tests[props.selected_test_case].result}</h3>

            <img src={Speedometer} width="100" height="100" alt="temp speedometer" />
            <h3 className="testcase-noncode">Runtime</h3>
            <h3 className="testcase-code">{props.output.data.tests[props.selected_test_case].exec_time} ms</h3>
            
            
            {!props.output.data.tests[props.selected_test_case].name.includes("Hidden") && (
            <h3 className="testcase-noncode">Input</h3>)}
            
            {!props.output.data.tests[props.selected_test_case].name.includes("Hidden") && (
            <h3 className="testcase-code">{props.output.data.tests[props.selected_test_case].input.replace("|", ", ")}</h3>)}
            
            {!props.output.data.tests[props.selected_test_case].name.includes("Hidden") && (
            <h3 className="testcase-noncode">Your Output</h3>)}

            {!props.output.data.tests[props.selected_test_case].name.includes("Hidden") && (
            <h3 className="testcase-code">{props.output.data.tests[props.selected_test_case].actual}</h3>)}
            
            {!props.output.data.tests[props.selected_test_case].name.includes("Hidden") && (
            <h3 className="testcase-noncode">Expected Output</h3>)}

            {!props.output.data.tests[props.selected_test_case].name.includes("Hidden") && (
            <h3 className="testcase-code">{props.output.data.tests[props.selected_test_case].expected}</h3>)}

        </div>}
        </>
    );
}

export default TestCaseDetails;

