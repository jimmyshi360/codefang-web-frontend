import React, { useState, useEffect } from "react";

function TestCase(props) {
  return (
    <div
      className={
        "testcase-block" +
        (props.compiled && props.output.result === "Passed"
          ? " testcase-pass"
          : " testcase-fail")
      }
    >
      {props.output.name}
    </div>
  );
}

export default TestCase;
