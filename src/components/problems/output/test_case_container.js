import React, { useState, useEffect } from "react";
import TestCase from "./test_case";

function TestCaseContainer(props) {
  const onClick = (index) => {
    props.setTestCase(index);
  };

  return (
    <div className="testcase-container">
      <ul className="testcase-ul">
        {props.output != undefined &&
          props.output.data.tests.map((item, index) =>
            props.selected_test_case == index ? (
              <li className="testcase-li-active" onClick={() => onClick(index)}>
                <TestCase output={item} compiled={props.output.compiled} />
              </li>
            ) : (
              <li className="testcase-li" onClick={() => onClick(index)}>
                <TestCase output={item} compiled={props.output.compiled} />
              </li>
            )
          )}
      </ul>
    </div>
  );
}

export default TestCaseContainer;
