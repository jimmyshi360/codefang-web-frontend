import React, { useState, useEffect } from 'react';
import axios from 'axios';
import Submission from './submission';

export default function Submissions(props) {

  const [arr, setArr] = useState();
  function getSubmissions(){
    axios(process.env.REACT_APP_BACKEND_URL + '/api/submissions', {
      method: 'get',
      headers: {
        'Content-Type': 'text/plain'
      },
      params: {
        user_id: props.userId,
        problem_id: props.selectedProblem,
      }
    })
    .then((response) => {
      console.log(props.userId);
      console.log(response);
      setArr(response);
    });
  };

  useEffect(() => getSubmissions(), []);

  return (
    <div className="submissions">

      {arr && arr.data.map((item) => (
          <Submission
            submissionTime={item.time}
            code={item.code}
          />
        ))}
    </div>
  );
}
