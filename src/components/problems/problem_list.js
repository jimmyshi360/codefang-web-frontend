import React, { useState, useEffect } from 'react';
import ProblemItem from './problem_item';
import axios from 'axios';

export default function ProblemList() {

  const [problems, setProblems] = useState([]);

  function getProblems() {
    axios(process.env.REACT_APP_BACKEND_URL + '/api/problems', {
      method: 'get',
      headers: {
        'Content-Type': 'text/plain',
      },
    }).then((response) => {
      setProblems(response.data);
    })
  }
  useEffect(() => getProblems(), []);


  return (
    
    <div className="problem-list-container">
      <table> 
        <tr className="problem-row">
          <th className="table-entry">  </th>
          <th className="table-entry"> # </th>
          <th className="table-entry"> Problem </th>
          <th className="table-entry"> Category </th>
          <th className="table-entry"> Acceptance </th>
          <th className="table-entry"> Difficulty </th>
        </tr>
        <tbody>
          {problems.map((problem) => (
            <ProblemItem
            title={problem.title}
            category={problem.category}
            acceptance={problem.acceptance}
            difficulty={problem.difficulty}
            number={problem.id}
            link={problem.link}
            completed={problem.completed}
            />
          ))}
        </tbody>
      </table>
          
    </div>
  );
}
