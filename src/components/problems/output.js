import React, { useState, useEffect } from 'react';
import Result from './result';
import Submission from './submission';
import axios from 'axios';
import TestCaseContainer from './output/test_case_container';
import TestCaseDetails from './output/test_case_details';

function Output(props) {


  const [arr, setArr] = useState();
  const [selected_test_case, setTestCase] = useState(0);

  function getSubmissions() {
    axios(process.env.REACT_APP_BACKEND_URL + '/api/submissions', {
      method: 'get',
      headers: {
        'Content-Type': 'text/plain'
      },
      params: {
        user_id: props.userId,
        problem_id: props.selectedProblem,
      }
    })
      .then((response) => {
        console.log(props.userId);
        console.log(response);
        setArr(response);
      });
  };

  useEffect(() => getSubmissions(), []);

  let delay = 0;
  return (
    <div className="output-container">
      <div className="testcase-results-container">
        <TestCaseContainer output={props.output}selected_test_case={selected_test_case} setTestCase={setTestCase} />
        <TestCaseDetails output={props.output} selected_test_case={selected_test_case}/>
      </div>
      {/*<div className="output" >

      <ul>

      {props.outputTitle}

      <li>
      {props.output!=undefined && props.output.data.tests.map((item) => (
          <Result
            output={item}
            delay={delay+=100}
            compiled={props.output.compiled}
          />
      ))}
      </li>

     </ul>
    </div>

    <div>
       <p>
      {props.output!=undefined && props.output.data.stdout}
      </p>
    </div>

    { arr && arr.data.map((item, index) => { return index<10? (
        
        <Submission
          submissionTime={item.time}
          code={item.code}
        />
      ) : (<></>) })}
);
}*/}
  
    </div>
  );
}

export default Output;

