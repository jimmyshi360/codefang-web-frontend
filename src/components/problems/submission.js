import React, { useState, useEffect } from 'react';
import axios from 'axios';
import { Controlled as CodeMirror } from 'react-codemirror2';
import 'codemirror/lib/codemirror.css';
import 'codemirror/mode/javascript/javascript';
import 'codemirror/mode/clike/clike';
import 'codemirror/mode/python/python';
import 'codemirror/mode/xml/xml';
import 'codemirror/mode/markdown/markdown';
import 'codemirror/addon/edit/closebrackets';

export default function Submission(props) {

  function renderExpansions(){
    let coll = document.getElementsByClassName("collapsible");
    for (let i = 0; i < coll.length; i++) {
      coll[i].addEventListener("click", function() {
        this.classList.toggle("active");
        let content = this.nextElementSibling;
        if (content.style.display === "block") {
          content.style.display = "none";
        } else {
          content.style.display = "block";
        }
      });
    }
  }

  useEffect(()=>{setTimeout(()=>{  renderExpansions()
  }, 1000)
}, []);
  return (
    <div className="submission">
      
      {props.submissionTime}

      <button type="button" class="collapsible quote-blue">View Code</button>
     

      <CodeMirror
          value={props.code}
          className="codemirror-container content"
          options={{
            lineNumbers: true,
            mode: 'text/x-java',
            lineWrapping: true,
            theme: 'monokai',
            tabMode: 'indent',
            autoMatchParens: true,
            smartIndent: true,
            tabSize: 4,
            indentUnit: 4,
            matchBrackets: true,
            autoCloseBrackets: true,
          }}
        />

    </div>
  );
}
