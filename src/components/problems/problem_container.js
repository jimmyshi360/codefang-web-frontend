import React, { useState, useEffect } from 'react';
import TutorialNavbar from './tutorial_navbar';
import Tutorial from './tutorial';
import Output from './output';
import Submissions from './submissions';
import axios from 'axios';
import Syllabus from '../syllabus';

function ProblemContainer(props) {

  const [scrollTop, setScrollTop] = useState(0);
  const [steps, setSteps] = useState([]);
  const [stepIndex, setStepIndex] = useState(0);

 
  function markTutorial(id) {
    axios(process.env.REACT_APP_BACKEND_URL + '/api/submissions', {
      method: 'post',
      headers: {
        'Content-Type': 'text/plain',
      },
      params: {
        user_id: props.userId,
        tutorial_id: id,
      },
    }).then((response) => {
      props.refreshPlayerData();
      console.log(response);
    });

  }

  function initializeHoldButton(){
    let duration = 1000,
    success = button => {
        //Success function
        button.classList.add('success');
    };

document.querySelectorAll('.button-hold').forEach(button => {
    button.style.setProperty('--duration', duration + 'ms');
    ['mousedown', 'touchstart', 'keypress'].forEach(e => {
        button.addEventListener(e, ev => {
            if(e != 'keypress' || (e == 'keypress' && ev.which == 32 && !button.classList.contains('process'))) {
                button.classList.add('process');
                button.timeout = setTimeout(success, duration, button);
            }
        });
    });
    ['mouseup', 'mouseout', 'touchend', 'keyup'].forEach(e => {
        button.addEventListener(e, ev => {
            if(e != 'keyup' || (e == 'keyup' && ev.which == 32)) {
                button.classList.remove('process');
                clearTimeout(button.timeout);
            }
        }, false);
    });
});
  }

  useEffect(() => {
    if (steps[stepIndex] > 1000) {
      props.setSelectedProblem(steps[stepIndex] - 1000);
      props.setSelectedTutorial(-1);
    }
    else {
      props.setSelectedTutorial(steps[stepIndex]);
      props.setSelectedProblem(-1);
    }
  }, [stepIndex, steps]);

  useEffect(()=> {initializeHoldButton()}, []);

  return (
    <div style={{display: 'flex', float: 'left', height: '100%', width: '100%'}}>
    <Syllabus
                  setProblemIndex={props.setProblemIndex}
                  problemIndex={props.problemIndex}
                  hamburgerClicked={props.hamburgerClicked}
                  headers={props.headers} 
                  setHeaders={props.setHeaders}
                  headerIndex={props.headerIndex}
                  progress={props.progress}
                  nodegroup={props.nodegroup}
                  nodegroupIds={props.nodegroupIds}
                  unfilteredId={props.unfilteredId}
                  setUnfilteredId={props.setUnfilteredId}
                  setSelectedProblem={props.setSelectedProblem}
                  setSelectedTutorial={props.setSelectedTutorial}
                  problemTitle={props.problemTitle}
                  setShowIDE={props.setShowIDE}
                />

    <div className={props.problemIndex!=2 ? "problem-container no-pointer-events" : "problem-container problem-container-pointer"}>
      <TutorialNavbar updateMiniGameWidth={props.updateMiniGameWidth} setLeftPanelTabIndex={props.setLeftPanelTabIndex} leftPanelTabIndex={props.leftPanelTabIndex} hamburgerClicked={props.hamburgerClicked} setHamburgerClicked={props.setHamburgerClicked} selectedProblem={props.selectedProblem} />
      {props.leftPanelTabIndex == 0
        && <Tutorial miniGameClicked={props.miniGameClicked} setMiniGameClicked={props.setMiniGameClicked} textLoading={props.textLoading} miniGameComplete={props.miniGameComplete} setGameOpen={props.setGameOpen} startGame={props.startGame} setRefreshHeaders={props.setRefreshHeaders} refreshHeaders={props.refreshHeaders} selectedProblem={props.selectedProblem} userId={props.userId} headerIndex={props.headerIndex} setHeaderIndex={props.setHeaderIndex} setScrollTop={setScrollTop} scrollTop={scrollTop} text={props.text} title={props.title} headers={props.headers} setHeaders={props.setHeaders} selectedTutorial={props.selectedTutorial} />
      }
      {props.leftPanelTabIndex == 2
        && <Output output={props.output} outputTitle={props.outputTitle} />
      }
      {props.leftPanelTabIndex == 3
        && <Submissions selectedProblem={props.selectedProblem} userId={props.userId} />
      }


      {/*
    <div style={{ width: '100%', zIndex: '100', height: '40px', backgroundColor: 'rgb(17, 21, 27)', color: 'white', display: 'flex' }}>
      
      {stepIndex>0 && <button onClick={() => {decrementClick();}}> Previous </button>}
      {steps.map((item, index) => (
        <div className={stepIndex == index ? "glow-block glow-active" : "glow-block"}></div>
      ))}
      {stepIndex<steps.length-1 && <button onClick={() => {incrementClick();}}>Next</button>}

    </div>
      */}
      <div style={{ width: '100%', zIndex: '100', height: '50px', backgroundColor: 'rgb(16, 18, 22)', color: 'white', display: 'flex' }}>
        

        <button onClick={() => {markTutorial(props.selectedTutorial);
      
          if(props.nodegroupIds[1]>=1000){
            props.setSelectedProblem(props.nodegroupIds[1]-1000);
            props.setSelectedTutorial(-1);
          }
          else{
            props.setSelectedProblem(-1);
            props.setSelectedTutorial(props.nodegroupIds[1]);
          }

          props.setUnfilteredId(props.nodegroupIds[1]);
          document.getElementById('tutorial-scroll').scrollTop=0;
      }
      } class="button-hold">
          <div>
            <svg class="icon" viewBox="0 0 16 16">
              <polygon points="1.3,6.7 2.7,8.1 7,3.8 7,16 9,16 9,3.8 13.3,8.1 14.7,6.7 8,0"></polygon>
            </svg>
            <svg class="progress" viewBox="0 0 32 32">
              <circle r="8" cx="16" cy="16" />
            </svg>
            <svg class="tick" viewBox="0 0 24 24">
              <polyline points="18,7 11,16 6,12" />
            </svg>
          </div>
            Next
        </button>
      </div>
    </div>
    </div>
  );
}

export default ProblemContainer;
