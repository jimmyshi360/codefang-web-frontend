import React, { useEffect } from 'react';
import { Sigma, RelativeSize, NodeShapes, ForceAtlas2, EdgeShapes } from 'react-sigma';

import Node from '../../imgs/node.png';
// #34a373, #f2b132, #e54647
const myGraph = {
  nodes: [{
    id: 'n1',
    label: 'Iterative Binary Search',
    x: 0,
    y: 0,
    color: '#34a373',
    size: '20',
    image: {
      url: Node,
      clip: 10.0,
      scale: 2.1,
    },
  },
  {
    id: 'n2',
    label: 'Recursive Binary Search',
    x: 0,
    y: 0,
    color: '#34a373',
    size: '20',
    image: {
      url: Node,
      clip: 10.0,
      scale: 2.1,
    },
  }, {
    id: 'n3',
    label: 'Insertion w/ Binary Search',
    x: 0,
    y: 0,
    color: '#34a373',
    size: '20',
    image: {
      url: Node,
      clip: 10.0,
      scale: 2.1,
    },
  },
  {
    id: 'n4',
    label: 'Find First Number',
    x: 0,
    y: 0,
    color: '#34a373',
    size: '20',
    image: {
      url: Node,
      clip: 10.0,
      scale: 2.1,
    },
  },
  {
    id: 'n5',
    label: 'Find Occurences of Number',
    x: 0,
    y: 0,
    color: '#f2b132',
    size: '20',
    image: {
      url: Node,
      clip: 10.0,
      scale: 2.1,
    },
  },
  {
    id: 'n6',
    label: 'Find Lowest Point in V',
    x: 0,
    y: 0,
    color: '#f2b132',
    size: '20',
    image: {
      url: Node,
      clip: 10.0,
      scale: 2.1,
    },
  },
  {
    id: 'n7',
    label: 'Search for Number in V',
    x: 0,
    y: 0,
    color: '#f2b132',
    size: '20',
    image: {
      url: Node,
      clip: 10.0,
      scale: 2.1,
    },
  },
  {
    id: 'n8',
    label: 'Final Challenge',
    x: 0,
    y: 0,
    color: '#e54647',
    size: '20',
    image: {
      url: Node,
      clip: 10.0,
      scale: 2.1,
    },
  },
  {
    id: 'n9',
    label: 'Search in Rotated Array',
    x: 0,
    y: 0,
    color: '#34a373',
    size: '20',
    image: {
      url: Node,
      clip: 10.0,
      scale: 2.1,
    },
  },
  ],

  edges: [{
    id: 'e1', source: 'n1', target: 'n2', label: 'SEES', size: '10',
  },
  {
    id: 'e2', source: 'n2', target: 'n3', label: 'SEES', size: '10',
  },
  {
    id: 'e4', source: 'n4', target: 'n5', label: 'SEES', size: '10', color: '#ffffff',
  },
  {
    id: 'e5', source: 'n4', target: 'n6', label: 'SEES', size: '10', color: '#ffffff',
  },
  {
    id: 'e6', source: 'n5', target: 'n7', label: 'SEES', size: '10', color: '#ffffff',
  },
  {
    id: 'e7', source: 'n6', target: 'n7', label: 'SEES', size: '10', color: 'grey',
  },
  {
    id: 'e8', source: 'n7', target: 'n8', label: 'SEES', size: '10', color: 'grey',
  },
  {
    id: 'e9', source: 'n3', target: 'n9', label: 'SEES', size: '10', color: 'grey',
  },
  ],
};

export default function Graph() {
  return (
    <div className="graph-container">
      <Sigma
        renderer="canvas"
        graph={myGraph}
        settings={{
          drawEdges: true,
          clone: false,
          defaultEdgeColor: 'white',
          edgeColor: 'default',
          defaultNodeColor: '#ffffff',
          defaultLabelColor: 'white',
          labelSize: 'proportional',
          labelThreshold: 0,
          minEdgeSize: 10,
          maxEdgeSize: 10,
          minNodeSize: 15,
          maxNodeSize: 15,
          enableEdgeHovering: true
        }}
        style={{ height: '100vh' }}
        onClickNode={(e) => { console.log(e); }}
        onClickEdge={(e) => { console.log(e); }}
      >
        <NodeShapes default="circle" />
        <EdgeShapes default="line" />
        <RelativeSize initialSize={15} />
        <ForceAtlas2/>
      </Sigma>
    </div>
  );
}
