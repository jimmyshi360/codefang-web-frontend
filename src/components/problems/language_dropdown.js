/* eslint-disable react/destructuring-assignment */
/* eslint-disable react/prop-types */
import React, {useState} from 'react';
import Dropdown from 'react-dropdown';
import 'react-dropdown/style.css';

export default function LanguageDropdown(props) {
  const [displayLang, setDisplayLang] = useState('Java');
  const changeLang = (e) => {
    setDisplayLang(e);
    switch (e.value) {
      case 'Java':
        props.setLang('java');
        break;
      case 'JavaScript':
        props.setLang('js');
        break;
      case 'C++':
        props.setLang('cpp');
        break;
      case 'Python 3':
        props.setLang('python');
        break;
      default:
        props.setCode('void');
    }
  };
  

  const langList = ["Java", "JavaScript", "C++", "Python 3"];

  return (

    <Dropdown options={langList} onChange={(e) => changeLang(e)} value={displayLang} placeholder="Select a language" />

  );
}
