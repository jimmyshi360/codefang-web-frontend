import React from 'react';
/*
 <div className="problem-item-container">
      <div className={`difficulty-marker d${props.difficulty}`} />
      <p className="problem-item-text">{props.title}</p>
    </div>
  */
export default function ProblemItem(props) {
  return (
    <tr className="problem-row">
      <td className="table-entry"> {props.completed} </td>
      <td className="table-entry"> {props.number} </td>
      <td>
        <div> 
          <a href="https://www.youtube.com/watch?v=oHg5SJYRHA0" className="problem-name"> {props.title} </a>
        </div>
      </td>
      <td className="table-entry"> {props.category} </td>
      <td className="table-entry"> {props.acceptance}% </td>
      <td className="table-entry"> {props.difficulty} </td>
    </tr>
  );
}
