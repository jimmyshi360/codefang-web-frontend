import React, { useState, useEffect } from 'react';
import 'pure-react-carousel/dist/react-carousel.es.css';
import CodeBlock from "./markdown/CodeBlock";
import ReactMarkdown from 'react-markdown/with-html';
import AOS from 'aos';
import 'aos/dist/aos.css';
import { CountUp } from 'countup.js';

import 'katex/dist/katex.min.css';
import { InlineMath, BlockMath } from 'react-katex';

import RemarkMathPlugin from 'remark-math';

function Tutorial(props) {

  AOS.init({ duration: 2000, once: true });
  function getHeaders(){
    const list = document.getElementsByTagName("h2");
    
    let nameList = [];
    for(let i=0; i<list.length; i++){
      nameList.push({innerHTML: list[i].innerHTML, offsetTop: list[i].offsetTop});
    }
    props.setHeaders(nameList);
  }

  function renderExpansions(){
    let coll = document.getElementsByClassName("collapsible");
    for (let i = 0; i < coll.length; i++) {
      coll[i].addEventListener("click", function() {
        this.classList.toggle("active");
        let content = this.nextElementSibling;
        
        if (content!=null && (!content.style.maxHeight|| content.style.maxHeight==0+"px" || content.style.display=="none")) {
          content.style.display = "block";
          content.style.maxHeight = content.scrollHeight+ "px";
        } else {
          content.style.maxHeight = 0 + "px";
        }
      });
    }
  }

  function listenButtons(){
    let playbutton = document.getElementById("search1-game");
    if(playbutton!=null){
      playbutton.addEventListener("click", function() {
        props.setGameOpen(true);
        props.setMiniGameClicked(true);
        props.startGame("StartSearch1Game");
      });
    }
  }

  function listenChecks(){
    let check = document.getElementById("game-checkbox");
    if(check!=null && props.miniGameComplete){
       check.classList.add("checked-box");
    }
  }

  function listenChecksClicked(){
    let check = document.getElementById("game-checkbox-launch");
    if(check!=null && props.miniGameClicked){
       check.classList.add("checked-box");
    }
  }

  function listenCounts(){
    let countElements=document.getElementsByClassName("count-span");
    
    for(let i=0; i<countElements.length; i++){
        let countUp = new CountUp(countElements[i], countElements[i].innerHTML, {duration: 4});
        countUp.start();
    }
  }

  const newProps = {
    ...props,
    plugins: [
      RemarkMathPlugin,
    ],
    renderers: {
      ...props.renderers,
      math: (props) =>
        <BlockMath math={props.value} />,
      inlineMath: (props) =>
        <InlineMath math={props.value} />,
        code: CodeBlock   
    }
  };

  useEffect(()=>{
    getHeaders();
    renderExpansions();
    listenButtons();
    listenCounts();
    listenChecksClicked();
    const observer = new IntersectionObserver(entries => {
      entries.forEach(entry => {
          const id = entry.target.getAttribute('id');
          const ulElem=document.querySelector(`ul li a[class="${id}"]`);
          if (entry.intersectionRatio > 0) {
              if(ulElem!=null)
                ulElem.parentElement.classList.add('syllabus-active');
          } else {
            if(ulElem!=null)
                ulElem.parentElement.classList.remove('syllabus-active');
          }
      });
  });

  // Track all sections that have an `id` applied
  document.querySelectorAll('section[id]').forEach((section) => {
      observer.observe(section);
  });

  }, [props.textLoading ]);

  useEffect(()=>{
    listenChecks()
  }, [props.miniGameComplete]);


  useEffect(()=>{
    listenChecksClicked()
  }, [props.miniGameClicked]);

  return (
    <div className="tutorial-container" id="tutorial-scroll" >
      
      {props.textLoading &&
      <div><div className="spinner"></div><div style={{textAlign: 'center'}}>Loading...</div></div>
      }
      {!props.textLoading && 
        <div className="tutorial-text-content"> 
        <ReactMarkdown {...newProps} style={{overflowY: 'scroll !important'}} source={props.text} escapeHtml={false}/>    
        </div>
      }
    </div>
  );
}

export default Tutorial;
