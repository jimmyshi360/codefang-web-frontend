
/* eslint-disable react/prop-types */
/* eslint-disable react/destructuring-assignment */
import React, { useState, useEffect } from 'react';
import { Controlled as CodeMirror } from 'react-codemirror2';
import 'codemirror/lib/codemirror.css';
import 'codemirror/mode/javascript/javascript';
import 'codemirror/mode/clike/clike';
import 'codemirror/mode/python/python';
import 'codemirror/mode/xml/xml';
import 'codemirror/mode/markdown/markdown';
import 'codemirror/addon/edit/closebrackets';
import LanguageDropdown from './language_dropdown';
import Request from 'axios-request-handler';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faCogs, faStopwatch } from '@fortawesome/free-solid-svg-icons'
require('../autorefresh.ext');


function IDE(props) {
  
  const [time, setTime] = useState(0);
  const [startTime, setStartTime] = useState(0);
  const [timerActive, setTimerActive] = useState(false);

  const parseTime = (time)=>{
    let date = new Date(null);
    date.setSeconds(time);
    return date.toISOString().substr(14, 5);
  }

  const toggleTimer = ()=>{
    setTimerActive(!timerActive);
    return;
  }

  const codemirrorLang = {
    js: "application/javascript",
    cpp: "text/x-c++src",
    java: "text/x-java",
    python: "text/x-python"
  }

  function filterLanguage() {
    switch (props.lang) {
      case 'python':
        return props.PYTHONCode;
        break;
      case 'cpp':
        return props.CPPCode;
        break;
      case 'java':
        return props.JAVACode;
        break;
      case 'js':
        return props.JSCode;
        break;
    }
  }

  function filterCodeSetter(){
    switch(props.lang){
      case 'python':
        return props.setPYTHONCode;
        break;
      case 'cpp':
        return props.setCPPCode;
        break;  
      case 'java':
        return props.setJAVACode;
        break;
      case 'js':
        return props.setJSCode;
        break;
    }
  }

  function runCode(e) {
    if(props.isLoggedIn){
      props.setLeftPanelTabIndex(2);
      e.preventDefault();
        console.log(props.currTopic+" "+props.problemTitle+" "+props.lang);
      //Create the run code request
      const runReq = new Request(process.env.REACT_APP_BACKEND_URL + '/api/kubernetes/run', {
        method: 'post',
        headers: {
          'Content-Type': 'text/plain'
        },
        params: {
          userId: props.userId,
          problemId: props.selectedProblem,
          problemTopic: (props.currTopic+"").replace(/\s/g, ''),
          problemName: (props.problemTitle+"").replace(/\s/g, ''),
          lang: props.lang
        },
        data: props.JAVACode
      });

      
      //Run the post request and get the executionId from the response if there's no error
      runReq.post().then((response) => {
        console.log(response);
        if(response.data.success) {
          //props.setOutput("A job has been created to run your code.");

          //Start polling for results
          pollResults(Date.now(), response.data.executionId, response.data.submissionId);
        } else {
          //props.setOutput("Error running your code.");
        }
      });
    }
    else{
      console.log("must log in to run code");
    }
  }

  function pollResults(startTime, executionId, submissionId) {

    //Create the polling request
    const pollReq = new Request(process.env.REACT_APP_BACKEND_URL + '/api/kubernetes/poll', {
      params: {
        submissionId: submissionId,
        userId: props.userId,
        executionId: executionId,
        lang: props.lang
      }
    });

    pollReq.poll(500).get((response) => {
      console.log(response);
      //Timeout condition: If it's been 8 seconds since the code started running, stop polling.
      if(Date.now() - startTime >= 12000) {
        props.setOutputTitle("Your code timed out.");
        return false;
      }

      if(response.data.complete) {

        //Cancel any pending poll requests, since this one is successful
        pollReq.cancel();

        let results = response.data.results;
        console.log(results);
        props.setOutput(results);
        props.setOutputTitle("Done");
        //Cancel polling since there's no longer a need to poll
        return false;

      } else {
        props.setOutputTitle("Waiting for results...");
      }
    }).catch((err) => {
      props.setOutputTitle("Error polling results: " + JSON.stringify(err));
    });
  }

  useEffect(() => {
    let interval = null;
    if (timerActive) {
      interval = setInterval(() => {
        setTime(time => time + 1);
      }, 1000);
    } else if (!timerActive && time !== 0) {
      clearInterval(interval);
    }
    return () => clearInterval(interval);
  }, [timerActive, time]);


  return (
    <div id="ide-container" onMouseEnter={()=> props.setHoverGame(true) } className={props.gameOpen ? "ide-container inactive-container" : "ide-container"}>
      <div className="codemirror-container" >
        <div className="codemirror-toolbar">
          {props.showIDE && 
          <>
          <p style={{margin: 0, color: 'white', padding: '5px 10px'}}>Language:</p>
          <LanguageDropdown setLang={props.setLang} lang={props.lang}/>
          
          <button className="submit-button" onClick={(e)=>runCode(e)}><FontAwesomeIcon icon={faCogs} style={{margin: 'auto', marginLeft: '0', marginRight: '5px', color: 'white'}}/>Submit</button>
          <button className="timer-button" onClick={()=>toggleTimer()}>
            <p className="timer-text">{parseTime(time)}</p>
            <FontAwesomeIcon icon={faStopwatch} style={{margin: 'auto', marginLeft: '0', marginRight: '5px', color: 'white'}}/>
          </button>
          </>
          }
        </div>

      
      {/*<MonacoEditor
        height="600"
        language="java"
        theme="vs-dark"
        value={filterLanguage()}
        options={{selectOnLineNumbers: true}}
        onChange={(value, e) => {filterCodeSetter()(value)}}
      />*/}

        {props.showIDE && 
        <CodeMirror
          value={filterLanguage()}
          className="codemirror-container"
          onBeforeChange={(editor, data, value) => {
            filterCodeSetter()(value);
          }}
          options={{
            lineNumbers: true,
            mode: codemirrorLang[props.lang],
            lineWrapping: true,
            theme: 'monokai',
            tabMode: 'indent',
            autoMatchParens: true,
            smartIndent: true,
            tabSize: 4,
            indentUnit: 4,
            matchBrackets: true,
            autoCloseBrackets: true,
            autoRefresh: {force: true},
          }}
        />
        }
         {!props.showIDE && !props.gameOpen && 

         <div className="codemirror-container" style={{backgroundColor: 'rgb(16, 18, 22)'}}> 

           </div>
      }
      </div>
    </div>
  );
}

export default IDE;
