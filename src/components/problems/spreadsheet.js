import React, { useState, useEffect } from 'react';
import { Remarkable } from 'remarkable';
import RemarkableReactRenderer from 'remarkable-react';
import { CarouselProvider, Slider, Slide, ButtonBack, ButtonNext, Dot } from 'pure-react-carousel';
import 'pure-react-carousel/dist/react-carousel.es.css';

export default function SpreadSheet(props) {
  
  let md = new Remarkable();
  md.renderer = new RemarkableReactRenderer();
  let textRendered = md.render(props.text);

  return (
    <div className="tutorial-container">
      <h2>{props.title}</h2>
      {textRendered}
    </div>
  );
}