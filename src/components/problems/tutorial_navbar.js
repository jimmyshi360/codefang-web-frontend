import React from 'react';
import { Link } from 'react-router-dom';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faBars } from '@fortawesome/free-solid-svg-icons'
import { HamburgerButton } from 'react-hamburger-button';

function TutorialNavbar(props) {
  
  return (
    <nav className="tutorial-navbar">
      <ul className="spec2" style={{margin: '0', height: '100%', padding: '0'}}>
        <li className="tutorial-li" style={{paddingLeft: '17px',
              paddingTop: '13px', paddingRight: '17px', cursor: 'pointer'}}>
            <HamburgerButton
              open={props.hamburgerClicked}
              onClick={() => {props.setHamburgerClicked(!props.hamburgerClicked);
                
                if(props.hamburgerClicked)
                  document.getElementById("syllabus").style.minWidth=0;
                else{
                  const pWidth=document.getElementById("__ReactUnityWebGL_1__").clientWidth;
                  document.getElementById("syllabus").style.minWidth=0.15*pWidth+"px";
                }
              }}
              width={16}
              height={11}
              strokeWidth={1}
              color='white'
              animationDuration={0.5}
            />
          </li>
          

        <li className={props.leftPanelTabIndex==0 ? "tutorial-li tl-active" : " tutorial-li"}>
          <Link className="tutorial-link" onClick={()=>props.setLeftPanelTabIndex(0)}>
          {props.selectedProblem==-1 ? 'Tutorial' : 'Description'}
          </Link>
        </li>

        {props.selectedProblem!=-1 &&
            (
            <>
            <li className={props.leftPanelTabIndex==1 ? "tutorial-li tl-active" : " tutorial-li"}>
              <Link className="tutorial-link" onClick={()=>props.setLeftPanelTabIndex(1)}>
              Solution
              </Link>
            </li>
            <li className={props.leftPanelTabIndex==2 ? "tutorial-li tl-active" : " tutorial-li"}>
              <Link className="tutorial-link" onClick={()=>props.setLeftPanelTabIndex(2)}>
              Output
              </Link>
            </li>
            <li className={props.leftPanelTabIndex==3 ? "tutorial-li tl-active" : " tutorial-li"}>
              <Link className="tutorial-link" onClick={()=>props.setLeftPanelTabIndex(3)}>
              Submissions
              </Link>
            </li>
            </>)
        }
      </ul>
    </nav>
  );
}

export default TutorialNavbar;
