import React, { useState, useEffect } from 'react';
import AOS from 'aos';
import 'aos/dist/aos.css';

export default function Result(props) {
  AOS.init({ duration: 2000, once: true });
 

  useEffect(()=>{console.log(props.output)})
  return (
    <div className={'result-container' + (props.compiled && props.output.result==="Passed" ? " result-pass" : " result-fail")} 
    data-aos="fade-up"
    data-aos-duration="1000"
    data-aos-delay={props.delay}>
      <h3>{props.output.input}</h3>
      <h3>{props.output.expected}</h3>
      <h3>{props.output.actual}</h3>
      <h3>{props.output.result}</h3>
      <h3>{props.output.exec_time}</h3>
    </div>
  );
}

