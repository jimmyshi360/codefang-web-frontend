import React, { useState, useEffect, useLayoutEffect } from 'react';
import IDE from './problems/ide';
import ProblemContainer from './problems/problem_container';
import SkillTreeOverlay from './problems/skill_tree_overlay';
import ProblemList from './problems/problem_list';
import Syllabus from './syllabus';
import AppBar from './app_bar';
import LeftBar from './left_bar';
import Encyclopedia from './problems/encyclopedia';
import axios from 'axios';

import Split from 'react-split'
import Unity, { UnityContent } from "react-unity-webgl";
import Community from './problems/community';
import ProblemLoadScreen from '../components/problem_load_screen';
import { useAlert } from 'react-alert'

const unityTreeContent = new UnityContent(
  "unity_project_build/Build/unity_project_build.json",
  "unity_project_build/Build/UnityLoader.js"
);

export default function Problems(props) {

  const [lang, setLang] = useState('java');
  const [JAVACode, setJAVACode] = useState("");
  const [PYTHONCode, setPYTHONCode] = useState("");
  const [JSCode, setJSCode] = useState("");
  const [CPPCode, setCPPCode] = useState("");

  const [problemText, setProblemText] = useState("");
  const [problemTitle, setProblemTitle] = useState("");
  const [output, setOutput] = useState(undefined);
  const [outputTitle, setOutputTitle] = useState('');
  const [leftPanelTabIndex, setLeftPanelTabIndex] = useState(0);
  const [hamburgerClicked, setHamburgerClicked] = useState(true);
  const [headers, setHeaders] = useState([]);
  const [headerIndex, setHeaderIndex] = useState(0);
  const [selectedProblem, setSelectedProblem] = useState(-1);
  const [selectedTutorial, setSelectedTutorial] = useState(-1);
  const [currTopic, setCurrTopic] = useState('BinarySearch');
  const [problemIndex, setProblemIndex] = useState(3);
  const [selectedNodeHeader, setSelectedNodeHeader] = useState(-1);

  const [progress, setProgress] = useState(0);
  const [nodegroup, setNodeGroup] = useState([]);
  const [nodegroupIds, setNodeGroupIds] = useState([]);
  const [unfilteredId, setUnfilteredId] = useState(-1);
  const [refreshHeaders, setRefreshHeaders] = useState(false);

  const [gameOpen, setGameOpen] = useState(false);
  const [hoverGame, setHoverGame] = useState(false);
  const [miniGameComplete, setMiniGameComplete] = useState(false);
  const alert = useAlert()
  const [textLoading, setTextLoading] = useState(false);
  const [showIDE, setShowIDE] = useState(false);
  const [miniGameClicked, setMiniGameClicked ] = useState(false);

  unityTreeContent.on('progress', progression => {
    setProgress(progression);
  });

  unityTreeContent.on('loaded', () => {
    setProgress(1);
  });

  unityTreeContent.on("SelectProblem", id => {
    if(id>=1000){
      setSelectedProblem(id-1000);
      setSelectedTutorial(-1);
      setShowIDE(true);
    }
    else{
      setSelectedProblem(-1);
      setSelectedTutorial(id);
      setShowIDE(false);
    }
    setUnfilteredId(id);
    setProblemIndex(2);
  });

  unityTreeContent.on("MarkMinigame", () => {
    setMiniGameComplete(true);
  });

  unityTreeContent.on("SelectHeader", id => {
    setSelectedNodeHeader(id);
  });

  unityTreeContent.on("SendString", string => {
    let json=JSON.parse(string);
    setNodeGroup(json.nodeNames);
    setNodeGroupIds(json.nodeIds);
  });

  unityTreeContent.on("SetGameOpen", state => {
    setGameOpen(state==1);
  });
  
  function setUnityViewState(state){
    unityTreeContent.send(
      "game-controller", 
      "setCameraMode", 
      state
    );
  }
  unityTreeContent.on("SetPointerState", state => {
    if(state==1){
      document.getElementById("#canvas").style.cursor = "pointer";
    }
    else if(state==2){
      document.getElementById("#canvas").style.cursor = "grabbing";
    }
    else{
      document.getElementById("#canvas").style.cursor = "default";
    }
  });
  
  const topics=["BinarySearch", "Arrays", "Strings", "Graphs", "Greedy", "DynamicProgramming", "HashTables", "StacksandQueues", "Heaps", "Trees"];
  
  unityTreeContent.on("SetTopic", state => {
    setCurrTopic(topics[state]);
  });

  function retrieveProblemText() {
    if(selectedProblem!=-1){
      setTextLoading(true);
      axios(process.env.REACT_APP_BACKEND_URL + '/api/problems', {
        method: 'get',
        headers: {
          'Content-Type': 'text/plain'
        },
        params: {
          id: selectedProblem
        }
      })
        .then((response) => {
          setProblemText(response.data[0].text);
          setProblemTitle(response.data[0].title);
          setRefreshHeaders(true);
          setTextLoading(false);
        });
    }
  };

  function startGame(gameName){
    unityTreeContent.send("game-controller", "setGameOpen", 1);
    unityTreeContent.send("game-controller", "SetModalWidth", document.getElementById("ide-container").offsetWidth+50);
    unityTreeContent.send("game-controller", gameName);
  }

  function refreshPlayerData(){
    unityTreeContent.send("game-controller", "RefreshPlayerData");
  }

  function retrieveTutorialText() {

    if(selectedTutorial!=-1){
      setTextLoading(true);
      axios(process.env.REACT_APP_BACKEND_URL + '/api/tutorials', {
        method: 'get',
        headers: {
          'Content-Type': 'text/plain'
        },
        params: {
          id: selectedTutorial
        }
      })
        .then((response) => {
          setProblemText(response.data[0].text);
          setProblemTitle(response.data[0].title);
          setRefreshHeaders(true);
          setTextLoading(false);
        });
    }
  };

  function retrieveIDEText() {
    axios(process.env.REACT_APP_BACKEND_URL + '/api/templates', {
      method: 'get',
      headers: {
        'Content-Type': 'text/plain'
      },
      params: {
        problem_id: selectedProblem
      }
    })
      .then((response) => {
        setJAVACode(response.data[0].java);
        setCPPCode(response.data[0].cpp);
        setPYTHONCode(response.data[0].python3);
        setJSCode(response.data[0].javascript);
      });
  };

  function updateMiniGameWidth(){
    unityTreeContent.send("game-controller", "SetModalWidth", document.getElementById("ide-container").offsetWidth);
  }

  useEffect(()=> {
    window.addEventListener('resize', function() {
      unityTreeContent.send("game-controller", "SetModalWidth", document.getElementById("ide-container").offsetWidth);
    });
  }, [])
  useEffect(() => {if(selectedProblem!=-1)retrieveProblemText()}, [selectedProblem]);
  useEffect(() => {if(selectedTutorial!=-1)retrieveTutorialText() 
    }, [selectedTutorial]);
  useEffect(() => {if(selectedProblem!=-1)
                      retrieveIDEText()
                    else{
                      setJAVACode("//Select a problem");
                      setCPPCode("//Select a problem");
                      setPYTHONCode("//Select a problem");
                      setJSCode("//Select a problem");
                    }}, [selectedProblem]); 
  useEffect(() => setProblemIndex(1), []);
  useEffect(()=>{
    /*
    WebGLInject - Part of Simple Spectrum V2.1 by Sam Boyer.
    */

    window.SimpleSpectrum = {};

    window.AudioContext = (function(){
      var ACConsructor = window.AudioContext || window.webkitAudioContext; //keep a reference to the original function
      
      //console.log('AudioContext Constructor overriden');
        
      return function(){
        var ac = new ACConsructor();

        //console.log('AudioContext constructed');
        
        window.SimpleSpectrum.ac = ac;

        window.SimpleSpectrum.a = ac.createAnalyser();
        window.SimpleSpectrum.a.smoothingTimeConstant = 0;
          
        window.SimpleSpectrum.fa = new Uint8Array(window.SimpleSpectrum.a.frequencyBinCount); //frequency array, size of frequencyBinCount
          
        window.SimpleSpectrum.la = new Uint8Array(window.SimpleSpectrum.a.fftSize); //loudness array, size of fftSize
          
        window.SimpleSpectrum.a.connect(ac.destination); //connect the AnalyserNode to the AudioContext's destination.
          
        ac.actualDestination = ac.destination; //keep a reference to the destination.
          
        Object.defineProperty(ac, 'destination', { //replace ac.destination with our AnalyserNode.
          value: window.SimpleSpectrum.a,
          writable: false
        });		
          
        return ac; //send our modified AudioContext back to Unity.
      }
    })();
  },[]);
  useEffect(() => props.setNavbarVisible(false), []);
  useEffect(() => {
    if(progress==1){

      unityTreeContent.send("game-controller", "SetPlayerId", props.userId);
      unityTreeContent.send(
        "game-controller", 
        "setIsRetina", 
        window.devicePixelRatio
      );
      unityTreeContent.send("game-controller", "SetModalWidth", document.getElementById("ide-container").offsetWidth+50);
    }
  },[props.userId, progress])
  useEffect(() => { 
    let val = problemIndex==2 ? 1 : 0;
    unityTreeContent.send("game-controller", "setMapOpen", val);
    unityTreeContent.send("game-controller", "setGameOpen", val);

  }, [problemIndex])

  function isRetinaDisplay() {
    if (window.matchMedia) {
        var mq = window.matchMedia("only screen and (min--moz-device-pixel-ratio: 1.3), only screen and (-o-min-device-pixel-ratio: 2.6/2), only screen and (-webkit-min-device-pixel-ratio: 1.3), only screen  and (min-device-pixel-ratio: 1.3), only screen and (min-resolution: 1.3dppx)");
        return (mq && mq.matches || (window.devicePixelRatio > 1)); 
    }
  }

  useEffect(()=>{unityTreeContent.send("game-controller", "setSelectedNodeId", unfilteredId); console.log(unfilteredId)}, [unfilteredId]);
  useEffect(()=>{console.log("dpi:"+document.getElementById("dpi").offsetHeight);}, []);
  useEffect(()=>{console.log("isRetina:"+isRetinaDisplay())
 
  
  console.log("resolution width:"+window.screen.width+"\nresolution height:"+window.screen.height);
    console.log("pixel ratio: "+window.devicePixelRatio);
}, []);

  useEffect(()=>{
    if(progress==1){
      if(props.isLoggedIn)
        alert.show('Login success. Welcome back to Algotree, '+props.name,{type: 'success'} );
      else
        alert.show('Please login. Loading example player progress data...');
    }

    },[progress, props.isLoggedIn]);
  
  return (
    <div style={{ height: '100%', overflowY: 'hidden', display: 'flex' }}>
      <div style={{ width: '100%' }}>
        
        <AppBar setNavbarVisible={props.setNavbarVisible}
          setProblemIndex={setProblemIndex}
          problemIndex={problemIndex} 
          setUnityViewState={setUnityViewState}
          name={props.name}
          email={props.email}
          userId={props.userId}/>

          <div id="dpi"></div>


       { progress!=1 && <ProblemLoadScreen progress={progress} setLoadScreen={props.setLoadScreen}/>} 
        <div className="problems-page">
          <div className="problems-container">

            {problemIndex == 0
              &&
              <Encyclopedia />
            }
            {problemIndex == 4
              &&
              (
                <Community/>
              )
            }

            {problemIndex == 1
              &&
              (
                <SkillTreeOverlay/>
              )
            }
            <LeftBar 
            setNavbarVisible={props.setNavbarVisible}
            setProblemIndex={setProblemIndex}
            problemIndex={problemIndex} 
            setUnityViewState={setUnityViewState}
            name={props.name}
            email={props.email}
            userId={props.userId}
            isLoggedIn={props.isLoggedIn}
            />

          {/* progress==1 &&
          <div class="achievement-banner">
            <div class="achievement-icon">
              <span class="icon"><span class="icon-trophy"></span></span>
            </div>
            <div class="achievement-text">
              <p class="achievement-notification">{props.isLoggedIn ? 'Welcome back to Algotree,'+ props.name : 'Please log in. Loading sample player data...'}</p>
              <p class="achievement-name"></p>
            </div>
          </div>
          */}  


            {/*
            <div style={{ pointerEvents: 'none', position: 'absolute', top:'0', left: '0', zIndex: '20', width: '100vw', height: '100vh', background: 'radial-gradient(circle, transparent 80%, rgb(0, 0, 0, 0.8) 150%)'}}>
           
            </div>
            */}
            <Unity unityContent={unityTreeContent} className={problemIndex!=3 ? "unity-view" : "unity-view-hidden"} />

            <div id="unity" style={{height: window.innerHeight-85+'px'}} className={problemIndex==2 ? ( gameOpen ? "atlas-code-container atlas-code-active no-pointer-events" : "atlas-code-container atlas-code-active") : "atlas-code-container pointer-events-none" }>
            
                <Split sizes={[50, 50]} minSize={[0, 0]} snapOffset={300} style={{ width: "95%" }} onDrag={()=> {
                  unityTreeContent.send("game-controller", "SetModalWidth", document.getElementById("ide-container").offsetWidth+50);
                  }}>
                  <ProblemContainer problemIndex={problemIndex} 
                  miniGameComplete={miniGameComplete} 
                  setHoverGame={setHoverGame} 
                  updateMiniGameWidth={updateMiniGameWidth} 
                  setGameOpen={setGameOpen} startGame={startGame} 
                  setRefreshHeaders={setRefreshHeaders} 
                  refreshHeaders={refreshHeaders} 
                  refreshPlayerData={refreshPlayerData} 
                  setSelectedTutorial={setSelectedTutorial} 
                  setSelectedProblem={setSelectedProblem} 
                  selectedNodeHeader={selectedNodeHeader} 
                  selectedProblem={selectedProblem} 
                  userId={props.userId} 
                  headerIndex={headerIndex} 
                  setHeaderIndex={setHeaderIndex} 
                  leftPanelTabIndex={leftPanelTabIndex} 
                  setLeftPanelTabIndex={setLeftPanelTabIndex} 
                  output={output} setOutput={setOutput}
                  outputTitle={outputTitle} 
                  text={problemText} 
                  title={problemTitle} 
                  hamburgerClicked={hamburgerClicked}
                   setHamburgerClicked={setHamburgerClicked} 
                   headers={headers} setHeaders={setHeaders} 
                   selectedTutorial={selectedTutorial}
                   setMiniGameClicked={setMiniGameClicked}
                   miniGameClicked={miniGameClicked}
                   setProblemIndex={setProblemIndex}
                   problemIndex={problemIndex}
                   setHeaders={setHeaders}
                   progress={progress}
                   nodegroup={nodegroup}
                  nodegroupIds={nodegroupIds}
                  unfilteredId={unfilteredId}
                  setUnfilteredId={setUnfilteredId}
                  problemTitle={problemTitle}
                  textLoading={textLoading}
                  setShowIDE={setShowIDE}
                   />

                  <IDE setLeftPanelTabIndex={setLeftPanelTabIndex} output={output} setOutput={setOutput} setOutputTitle={setOutputTitle} lang={lang} setLang={setLang}
                    JAVACode={JAVACode} setJAVACode={setJAVACode}
                    JSCode={JSCode} setJSCode={setJSCode}
                    PYTHONCode={PYTHONCode} setPYTHONCode={setPYTHONCode}
                    CPPCode={CPPCode} setCPPCode={setCPPCode}
                    currTopic={currTopic}
                    problemTitle={problemTitle}
                    userId={props.userId} 
                    isLoggedIn={props.isLoggedIn}
                    selectedProblem={selectedProblem}
                    gameOpen={gameOpen}
                    hoverGame={hoverGame}
                    setHoverGame={setHoverGame}
                    showIDE={showIDE}
                    />

                </Split>
                {/*<div style={{ position: 'fixed', bottom: '0px', width: '50vw', zIndex: '100', height: '40px', backgroundColor: '#2d2d2d' }}>
                  <Stepper activeStep={activeStep} connector={<ColorlibConnector />} style={{ backgroundColor: '#2d2d2d' }}>
                    {steps.map(label => (
                      <Step key={label} >
                        <StepLabel StepIconComponent={ColorlibStepIcon}>{label}</StepLabel>
                      </Step>
                    ))}
                  </Stepper>
                    </div>*/}
              </div>
            
            {problemIndex == 3
              && <ProblemList />}
          </div>
        </div>
      </div>
    </div>
  );
}
