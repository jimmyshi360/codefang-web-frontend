import React, {useState, useEffect} from 'react';
import { List } from '@material-ui/core';

function Syllabus(props) {

  function setSyllabusHeight(){
    const pHeight=document.getElementById("__ReactUnityWebGL_1__").clientHeight;
    const pWidth=document.getElementById("__ReactUnityWebGL_1__").clientWidth;
    document.getElementById("syllabus").style.maxHeight=(pHeight-0.15*pWidth-15)+"px";
    document.getElementById("syllabus").style.minWidth=0.15*pWidth+"px";
    document.getElementById("syllabus").style.width=0.15*pWidth+"px";
  }

  function listenWindowResize(){
    window.addEventListener("resize", setSyllabusHeight);
  }

  function filterTitle(title){
    const dashIndex=title.indexOf("-");
    return title.substring(0, dashIndex);
  }
  useEffect(()=>{if(props.progress==1) setSyllabusHeight()}, [props.progress]);
  useEffect(()=>{listenWindowResize()}, []);

  return (
    <div id="syllabus"  className={props.problemIndex!=2 ? "syllabus-container no-pointer-events" : (props.hamburgerClicked ? "syllabus-container problem-container-pointer" : "syllabus-container syllabus-hidden problem-container-pointer")} > 
      <div className="syllabus-header">Table of Contents{/*filterTitle(props.problemTitle)*/}</div>
      
      <ul className="syllabus-list">
      {props.nodegroup.map((item, index) => (
          <li key={item}  className={props.nodegroupIds[index]==props.unfilteredId ? "syllabus-active-parent syllabus-link" : "syllabus-link"} onClick={()=>{
            if(props.nodegroupIds[index]>=1000){
              props.setSelectedProblem(props.nodegroupIds[index]-1000);
              props.setSelectedTutorial(-1);
              props.setShowIDE(true);
            }
            else{
              props.setSelectedProblem(-1);
              props.setSelectedTutorial(props.nodegroupIds[index]);
              props.setShowIDE(false);
            }

            props.setUnfilteredId(props.nodegroupIds[index]);
            document.getElementById('tutorial-scroll').scrollTop=0;
          }}>
           
            {(index+1)+". "+item}
            {props.nodegroupIds[index]==props.unfilteredId && 
            <ul className="inner-list" >
            {props.headers.map((item, index) => (
                <li  key={item.innerHTML} className={" syllabus-link" }>
                  <a class={item.innerHTML.replace(/\s/g, '')} onClick={()=>{document.getElementById('tutorial-scroll').scrollTo({top: item.offsetTop-80, left: 0, behavior: 'smooth'}); }}> {item.innerHTML} </a>
                  
                </li>
              ))}
            </ul>
            }
          </li>
        ))}
      </ul>
    </div>
  );
}

export default Syllabus;
