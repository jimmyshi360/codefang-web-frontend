/* eslint-disable jsx-a11y/label-has-associated-control */
import React, { useState, useEffect } from 'react';
import axios from 'axios';
import { Sigma, RelativeSize, NodeShapes, EdgeShapes } from 'react-sigma';
import { makeStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import Typography from '@material-ui/core/Typography';
import TextField from '@material-ui/core/TextField';
import { embedProps } from 'react-sigma/lib/tools';
import 'react-sigma/sigma/main';
import 'react-sigma/sigma/parsers.json';
import Button from '@material-ui/core/Button';
import { Controlled as CodeMirror } from 'react-codemirror2';
import 'codemirror/lib/codemirror.css';
import 'codemirror/mode/javascript/javascript';
import 'codemirror/mode/clike/clike';
import 'codemirror/mode/python/python';
import 'codemirror/mode/xml/xml';
import 'codemirror/mode/markdown/markdown';
import 'codemirror/addon/edit/closebrackets';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import Fab from '@material-ui/core/Fab';
import AddIcon from '@material-ui/icons/Add';
import TrashIcon from '@material-ui/icons/Delete';
import CreateProblemModal from './admin/create_problem_modal';
import CreateTutorialModal from './admin/create_tutorial_modal';
import LanguageDropdown from './admin/language_dropdown';
import IDEBlock from './admin/ide_block';
import DialogTitle from '@material-ui/core/DialogTitle';
import Dialog from '@material-ui/core/Dialog';
import Snackbar from '@material-ui/core/Snackbar';
import Alert from '@material-ui/lab/Alert';
import CodeBlock from "./problems/markdown/CodeBlock";
import ReactMarkdown from 'react-markdown';

const useStyles = makeStyles(theme => ({
  root: {
    flexGrow: 1,
    backgroundColor: theme.palette.background.paper,
  },
}));

export default function Admin(props) {
  const [topicList, setTopicList] = useState([]);
  const [topicName, setTopicName] = useState('');
  const [currTopic, setCurrTopic] = useState({ id: 1, title: "Binary Search" });

  const [JAVATemplateCode, setJAVATemplateCode] = useState('// Code\nclass Example {\n\tpublic int[] problemTitle(int param1, int[] param2) {\n\n\n\t}\n}');
  const [PYTHONTemplateCode, setPYTHONTemplateCode] = useState('// Code\nclass Solution:\n\tdef twoSum(self, nums: List[int], target: int) -> List[int]:\n\n\n');
  const [CPPTemplateCode, setCPPTemplateCode] = useState('// Code\nclass Solution {\npublic:\n\tvector<int> problemTitle(vector<int>& nums, int target) {\n\n\n\t}\n};');
  const [JSTemplateCode, setJSTemplateCode] = useState('// Code\nvar problemTitle = function(param1, param2) {\n\n\n\n};');

  const [JAVATestCode, setJAVATestCode] = useState('// Code\nclass Example {\n\tpublic int[] problemTitle(int param1, int[] param2) {\n\n\n\t}\n}');
  const [PYTHONTestCode, setPYTHONTestCode] = useState('// Code\nclass Solution:\n\tdef twoSum(self, nums: List[int], target: int) -> List[int]:\n\n\n');
  const [CPPTestCode, setCPPTestCode] = useState('// Code\nclass Solution {\npublic:\n\tvector<int> problemTitle(vector<int>& nums, int target) {\n\n\n\t}\n};');
  const [JSTestCode, setJSTestCode] = useState('// Code\nvar problemTitle = function(param1, param2) {\n\n\n\n};');
  const [dataFile, setDataFile] = useState('//test data');

  const [lang, setLang] = useState('java');
  const [problemList, setProblemList] = useState([]);
  const [tutorialList, setTutorialList] = useState([]);
  const [currProblem, setCurrProblem] = useState('');
  const [currTutorial, setCurrTutorial] = useState('');
  const [problemTitle, setProblemTitle] = useState('');
  const [problemDescription, setProblemDescription] = useState('');
  const [problemSolution, setProblemSolution] = useState('');

  const [tutorialTitle, setTutorialTitle] = useState('');
  const [tutorialText, setTutorialText] = useState('');
  const [createProblemModalVisible, setCreateProblemModalVisible] = useState(false);
  const [createTopicModalVisible, setCreateTopicModalVisible] = useState(false);
  const [createTutorialModalVisible, setCreateTutorialModalVisible] = useState(false);

  const [alertOpen, setAlertOpen] = useState(false);
  const [alertText, setAlertText] = useState('');
  const [isError, setIsError] = useState(false);

  //The actual graph. Contains all the nodes and edges
  const [nodeGraph, setNodeGraph] = useState('');

  function filterLanguageTemplate() {
    switch (lang) {
      case 'python':
        return PYTHONTemplateCode;
        break;
      case 'cpp':
        return CPPTemplateCode;
        break;
      case 'java':
        return JAVATemplateCode;
        break;
      case 'js':
        return JSTemplateCode;
        break;
    }
  }

  function filterLanguageSetterTemplate() {
    switch (lang) {
      case 'python':
        return setPYTHONTemplateCode;
        break;
      case 'cpp':
        return setCPPTemplateCode;
        break;
      case 'java':
        return setJAVATemplateCode;
        break;
      case 'js':
        return setJSTemplateCode;
        break;
    }
  }

  function filterLanguageTest() {
    switch (lang) {
      case 'python':
        return PYTHONTestCode;
        break;
      case 'cpp':
        return CPPTestCode;
        break;
      case 'java':
        return JAVATestCode;
        break;
      case 'js':
        return JSTestCode;
        break;
    }
  }

  function filterLanguageSetterTest() {
    switch (lang) {
      case 'python':
        return setPYTHONTestCode;
        break;
      case 'cpp':
        return setCPPTestCode;
        break;
      case 'java':
        return setJAVATestCode;
        break;
      case 'js':
        return setJSTestCode;
        break;
    }
  }

  function getTopics() {
    axios(process.env.REACT_APP_BACKEND_URL + '/api/topics', {
      method: 'get',
      headers: {
        'Content-Type': 'text/plain',
      },
    }).then((response) => {
      setTopicList(response.data);
    }).catch(error => {
      setAlertText(error + "");
      setIsError(true);
      setAlertOpen(true);
    });
  }

  function postTopic(e) {
    axios(process.env.REACT_APP_BACKEND_URL + '/api/topics', {
      method: 'post',
      headers: {
        'Content-Type': 'text/plain',
      },
      params: {
        title: topicName,
        adminPass: global.adminPassword
      },
    }).then((response) => {
      getTopics()
      setAlertText("Topic successfully added");
      setIsError(false);
      setAlertOpen(true);
    }).catch(error => {
      setAlertText(error + "");
      setIsError(true);
      setAlertOpen(true);
    });
  }

  function getProblems() {
    axios(process.env.REACT_APP_BACKEND_URL + '/api/problems', {
      method: 'get',
      headers: {
        'Content-Type': 'text/plain',
      },
    }).then((response) => {
      setProblemList(response.data);
    }).catch(error => {
      setAlertText(error + "");
      setIsError(true);
      setAlertOpen(true);
    });
  }

  function getUsers() {
    axios(process.env.REACT_APP_BACKEND_URL + '/api/users', {
      method: 'get',
      headers: {
        'Content-Type': 'text/plain',
      },
    }).then((response) => {
      console.log(response);
    }).catch(error => {
      setAlertText(error + "");
      setIsError(true);
      setAlertOpen(true);
    });
  }


  function getTutorials() {
    axios(process.env.REACT_APP_BACKEND_URL + '/api/tutorials', {
      method: 'get',
      headers: {
        'Content-Type': 'text/plain',
      },
    }).then((response) => {
      setTutorialList(response.data);
    }).catch(error => {
      setAlertText(error + "");
      setIsError(true);
      setAlertOpen(true);
    });
  }

  function deleteProblem() {
    axios(process.env.REACT_APP_BACKEND_URL + '/api/problems', {
      method: 'delete',
      headers: {
        'Content-Type': 'text/plain',
      },
      params: {
        id: currProblem.id,
        adminPass: global.adminPassword
      }
    }).then((response) => {
      setAlertText("Problem successfully deleted");
      setIsError(false);
      setAlertOpen(true);
      getProblems();
    }).catch(error => {
      setAlertText(error + "");
      setIsError(true);
      setAlertOpen(true);
    });
  }

  function patchProblemTitle() {
    axios(process.env.REACT_APP_BACKEND_URL + '/api/problems', {
      method: 'patch',
      headers: {
        'Content-Type': 'text/plain',
      },
      params: {
        id: currProblem.id,
        title: problemTitle,
        adminPass: global.adminPassword
      },
    }).then((response) => {
      setAlertText("Problem title successfully patched");
      setIsError(false);
      setAlertOpen(true);
    }).catch(error => {
      setAlertText(error + "");
      setIsError(true);
      setAlertOpen(true);
    });
  }

  function patchTutorialTitle() {
    axios(process.env.REACT_APP_BACKEND_URL + '/api/tutorials', {
      method: 'patch',
      headers: {
        'Content-Type': 'text/plain',
      },
      params: {
        id: currTutorial.id,
        title: tutorialTitle,
        adminPass: global.adminPassword
      },
    }).then((response) => {
      setAlertText("Tutorial title successfully patched");
      setIsError(false);
      setAlertOpen(true);
    }).catch(error => {
      setAlertText(error + "");
      setIsError(true);
      setAlertOpen(true);
    });
  }

  function patchTutorialText() {
    axios(process.env.REACT_APP_BACKEND_URL + '/api/tutorials', {
      method: 'patch',
      headers: {
        'Content-Type': 'application/json',
      },
      params: {
        id: currTutorial.id,
        title: tutorialTitle,
        adminPass: global.adminPassword
      },
      data:{
        text: tutorialText,
      }
    }).then((response) => {
      setAlertText("Tutorial text successfully patched");
      setIsError(false);
      setAlertOpen(true);
    }).catch(error => {
      console.log(error);
      setAlertText(error + "");
      setIsError(true);
      setAlertOpen(true);
    });
  }

  function patchDataFile() {
    axios(process.env.REACT_APP_BACKEND_URL + '/api/kubernetes/settest', {
      method: 'get',
      headers: {
        'Content-Type': 'text/plain',
      },
      params: {
        problemId: currProblem.id,
        problemTopic: currTopic.title.replace(/\s/g, ''),
        problemName: problemTitle,
        lang: lang,
        dataFile: dataFile,
        adminPass: global.adminPassword
      },
    }).then((response) => {
      setAlertText("Data file successfully patched");
      setIsError(false);
      setAlertOpen(true);
    }).catch(error => {
      setAlertText(error + "");
      setIsError(true);
      setAlertOpen(true);
    });
  }

  function patchProblemTemplateCode() {
    axios(process.env.REACT_APP_BACKEND_URL + '/api/templates', {
      method: 'patch',
      headers: {
        'Content-Type': 'text/plain',
      },
      params: {
        problem_id: currProblem.id,
        java: JAVATemplateCode,
        python3: PYTHONTemplateCode,
        cpp: CPPTemplateCode,
        javascript: JSTemplateCode,
        adminPass: global.adminPassword
      },
    }).then((response) => {
      setAlertText("Problem template successfully patched");
      setIsError(false);
      setAlertOpen(true);
    }).catch(error => {
      setAlertText(error + "");
      setIsError(true);
      setAlertOpen(true);
    });
  }

  function patchProblemTestCode() {
    axios(process.env.REACT_APP_BACKEND_URL + '/api/kubernetes/settest', {
      method: 'post',
      headers: {
        'Content-Type': 'application/json',
      },
      // TODO add more fields
      params: {
        problemId: currProblem.id,
        problemTopic: currTopic.title.replace(/\s/g, ''),
        problemName: problemTitle.replace(/\s/g, ''),
        dataFile: dataFile,
        adminPass: global.adminPassword
      },
      data: {
        java: JAVATestCode,
        python: PYTHONTestCode,
        javascript: JSTestCode,
        cpp: CPPTestCode
      }
    }).then((response) => {
      console.log(response);
      setAlertText("Problem test code successfully patched");
      setIsError(false);
      setAlertOpen(true);
    }).catch(error => {
      setAlertText(error + "");
      setIsError(true);
      setAlertOpen(true);
    });
  }

  function patchProblemDescription() {
    axios(process.env.REACT_APP_BACKEND_URL + '/api/problems', {
      method: 'patch',
      headers: {
        'Content-Type': 'text/plain',
      },
      params: {
        id: currProblem.id,
        text: problemDescription,
        adminPass: global.adminPassword
      },
    }).then((response) => {
      setAlertText("Problem description successfully patched");
      setIsError(false);
      setAlertOpen(true);
    }).catch(error => {
      setAlertText(error + "");
      setIsError(true);
      setAlertOpen(true);
    });
  }

  function patchProblemSolution() {
    console.log("patching")
    axios(process.env.REACT_APP_BACKEND_URL + '/api/problems', {
      method: 'patch',
      headers: {
        'Content-Type': 'text/plain',
      },
      params: {
        id: currProblem.id,
        solution: problemSolution,
        adminPass: global.adminPassword
      },
    }).then((response) => {
      console.log("patched")
      setAlertText("Problem solution successfully patched");
      setIsError(false);
      setAlertOpen(true);
    }).catch(error => {
      console.log(error)
      setAlertText(error + "");
      setIsError(true);
      setAlertOpen(true);
    });
  }

  function loadProblem(event) {
    const problem = event.target.value;

    setCurrProblem(problem);
    setProblemTitle(problem.title);
    if (problem.description == null)
      problem.description = '';
    setProblemDescription(problem.text);
    setProblemSolution(problem.solution);

    axios(process.env.REACT_APP_BACKEND_URL + '/api/templates', {
      method: 'get',
      headers: {
        'Content-Type': 'text/plain',
      },
      params: {
        problem_id: problem.id,
      }
    }).then((response) => {
      setCPPTemplateCode(response.data[0].cpp);
      setJAVATemplateCode(response.data[0].java);
      setJSTemplateCode(response.data[0].javascript);
      setPYTHONTemplateCode(response.data[0].python3);
    }).catch(error => {
      console.log(error);
    });

    axios(process.env.REACT_APP_BACKEND_URL + '/api/kubernetes/gettest', {
      method: 'get',
      headers: {
        'Content-Type': 'text/plain',
      },
      params: {
        problemId: problem.id,
        adminPass: global.adminPassword
      }
    }).then((response) => {
      console.log(response);
      setJAVATestCode(response.data.results.java);
      setPYTHONTestCode(response.data.results.python);
      setJSTestCode(response.data.results.javascript);
      setCPPTestCode(response.data.results.cpp);
      setDataFile(response.data.dataFile);
    }).catch(error => {
      console.log(error);
    });
  }

  function loadTutorial(event) {
    const tutorial = event.target.value;
    console.log(tutorial);
    setCurrTutorial(tutorial);
    setTutorialTitle(tutorial.title);
    if (tutorial.text == null)
      tutorial.tex = '';
    setTutorialText(tutorial.text);
  }

  const myGraph = {
    nodes: [{
      id: 'n1',
      label: 'Iterative Binary Search',
      x: 0,
      y: 0,
      color: '#34a373',
      size: '20',
      image: {
        url: Node,
        clip: 10.0,
        scale: 2.1,
      },
    },
    {
      id: 'n2',
      label: 'Recursive Binary Search',
      x: 10,
      y: 0,
      color: '#34a373',
      size: '20',
      image: {
        url: Node,
        clip: 10.0,
        scale: 2.1,
      },
    }, {
      id: 'n3',
      label: 'Insertion w/ Binary Search',
      x: 20,
      y: 0,
      color: '#34a373',
      size: '20',
      image: {
        url: Node,
        clip: 10.0,
        scale: 2.1,
      },
    },
    {
      id: 'n4',
      label: 'Find First Number',
      x: 30,
      y: 0,
      color: '#34a373',
      size: '20',
      image: {
        url: Node,
        clip: 10.0,
        scale: 2.1,
      },
    },
    {
      id: 'n5',
      label: 'Find Occurences of Number',
      x: 40,
      y: 0,
      color: '#f2b132',
      size: '20',
      image: {
        url: Node,
        clip: 10.0,
        scale: 2.1,
      },
    },
    {
      id: 'n6',
      label: 'Find Lowest Point in V',
      x: 50,
      y: 0,
      color: '#f2b132',
      size: '20',
      image: {
        url: Node,
        clip: 10.0,
        scale: 2.1,
      },
    },
    {
      id: 'n7',
      label: 'Search for Number in V',
      x: 60,
      y: 0,
      color: '#f2b132',
      size: '20',
      image: {
        url: Node,
        clip: 10.0,
        scale: 2.1,
      },
    },
    {
      id: 'n8',
      label: 'Final Challenge',
      x: 70,
      y: 0,
      color: '#e54647',
      size: '20',
      image: {
        url: Node,
        clip: 10.0,
        scale: 2.1,
      },
    },
    {
      id: 'n9',
      label: 'Search in Rotated Array',
      x: 0,
      y: 40,
      color: '#34a373',
      size: '20',
      image: {
        url: Node,
        clip: 10.0,
        scale: 2.1,
      },
    },
    ],

    edges: [{
      id: 'e1', source: 'n1', target: 'n2', label: 'SEES', size: '10', type: 'arrow'
    },
    {
      id: 'e2', source: 'n2', target: 'n3', label: 'SEES', size: '10', type: 'arrow'
    },
    {
      id: 'e3', source: 'n1', target: 'n3', label: 'SEES', size: '10', type: 'arrow'
    },
    {
      id: 'e4', source: 'n4', target: 'n5', label: 'SEES', size: '10', color: '#ffffff', type: 'arrow'
    },
    {
      id: 'e5', source: 'n4', target: 'n6', label: 'SEES', size: '10', color: '#ffffff', type: 'arrow'
    },
    {
      id: 'e6', source: 'n5', target: 'n7', label: 'SEES', size: '10', color: '#ffffff', type: 'arrow'
    },
    {
      id: 'e7', source: 'n6', target: 'n7', label: 'SEES', size: '10', color: 'grey', type: 'arrow'
    },
    {
      id: 'e8', source: 'n7', target: 'n8', label: 'SEES', size: '10', color: 'grey', type: 'arrow'
    },
    {
      id: 'e9', source: 'n3', target: 'n9', label: 'SEES', size: '10', color: 'grey', type: 'arrow'
    },
    {
      id: 'e10', source: 'n1', target: 'n4', label: 'SEES', size: '10', color: 'grey', type: 'arrow'
    }
    ],
  };

  // Modify x y positions of objects by expanding from root
  function layoutGraph(graphData) {
    let graph = new Map();

    let noEdges = (graphData.edges.length === 0);

    // Convert to map, where each node has adj. list
    graphData.nodes.forEach((node) => {

      if (noEdges) {
        console.log("No edges");
        node.x = Math.floor(Math.random() * (200));
        node.y = Math.floor(Math.random() * (200));
      }

      graph.set(node.id, {
        node, // This is a reference to the actual node object
        visited: false,
        inEdges: [],
        outEdges: []
      });

    });

    if (noEdges) {
      return graphData;
    }

    graphData.edges.forEach((edge) => {
      graph.get(edge.source).outEdges.push(edge.target);
      graph.get(edge.target).inEdges.push(edge.source);
    });

    let left = graph.size;
    let queue = [];
    let nextQueue = [];
    let xOffset = 0

    // Run BFS (sort of) on graph
    // Starts at root, adds all nodes from root to a different array. Then
    // goes through that array, adds all nodes (unless visited). For each level
    // we increase the xOffset and also layout all nodes vertically
    while (left > 0) {
      // Start at any (non visited) node with no incoming edges
      if (queue.length == 0) {
        for (let [id, node] of graph) {
          if (node.inEdges.length == 0 && !node.visited) {
            queue.push(node);
            break;
          }
        }

        if (queue.length == 0) {
          throw ("Error generating graph");
        }
      }

      // Index in current level (distance from root)
      let i = 0;
      while (queue.length > 0) {
        let curr = queue.pop();
        if (curr.visited) continue;
        curr.visited = true;
        left--;

        curr.node.x = xOffset;
        curr.node.y = i * 100 + xOffset / 50;

        for (const nodeId of curr.outEdges) {
          nextQueue.push(graph.get(nodeId));
        }
        i++;
      }

      queue = [...nextQueue];
      nextQueue = [];
      xOffset += 100;
    }

    return graphData;
  }

  //Build the nodes and edges for a graph state object
  function buildGraphObj(graphData, problemData, tutorialData) {
    let graphNodes = [];
    let graphEdges = [];
    console.log(graphData);
    //Iterate through the nodes retrieved from the database
    graphData.nodes.forEach(function (nodeObj) {
      //Create a node object for the graph based on the data of the node from the database,
      //and add it to the array we are building up
      let nodeLabel
      if(nodeObj.problem_id!=null)
        nodeLabel =nodeObj.id+": "+ problemData.find(p => p.id === nodeObj.problem_id).title;
      else
        nodeLabel = nodeObj.id+": "+ tutorialData.find(p => p.id === nodeObj.tutorial_id).title;
      graphNodes.push({
        id: 'n' + nodeObj.id,
        label: nodeLabel,
        x: 0,
        y: 0,
        color: '#34a373',
        size: '20',
        image: {
          url: Node,
          clip: 10.0,
          scale: 2.1,
        }
      });
    });
    graphData.edges.forEach(function (edgeObj) {
      graphEdges.push({
        id: 'e' + edgeObj.id,
        source: 'n' + edgeObj.from_node_id,
        target: 'n' + edgeObj.to_node_id,
        label: 'SEES',
        size: '10',
        type: 'arrow'
      });
    });

    return {
      nodes: graphNodes,
      edges: graphEdges
    };

  }

  //Loads an example, predefined graph (myGraph)
  function loadExampleGraph() {
    setNodeGraph(layoutGraph(myGraph));
  }

  // Load nodes and problems from the database and apply them to the sigma graph
  // (this function uses buildGraphObj as a helper method)
  function loadGraph() {
    let problemMap = [];
    let tutorialMap= [];
    axios(process.env.REACT_APP_BACKEND_URL + '/api/graph', {
      method: 'get',
      headers: {
        'Content-Type': 'application/json',
      },
      params: {
        topic_id: currTopic.id
      }
    })
      .then((graphResponse) => {
        axios(process.env.REACT_APP_BACKEND_URL + '/api/problems', { 
          method: 'get',
          headers: {
            'Content-Type': 'application/json'
          }
        })
          .then((problemResponse) => {
            problemMap = problemMap.concat(problemResponse.data);

            axios(process.env.REACT_APP_BACKEND_URL + '/api/tutorials', {
              method: 'get',
              headers: {
                'Content-Type': 'application/json'
              }
            })
              .then((problemResponse2) => {
                tutorialMap = tutorialMap.concat(problemResponse2.data);

                let graphObj = buildGraphObj(graphResponse.data, problemMap, tutorialMap);
                setNodeGraph(layoutGraph(graphObj));
              })
          })
          .catch(error => {
            console.log(error);
          });

      })
      .catch(error => {
        console.log(error)
      });
  }

  function deleteNode(id) {
    axios(process.env.REACT_APP_BACKEND_URL + '/api/nodes', {
      method: 'delete',
      headers: {
        'Content-Type': 'application/json'
      },
      // TODO tutorials?
      params: {
        id: id,
        adminPass: global.adminPassword
      },
    }).then((response) => {
      // alert("Node added. Reloading graph.");
      loadGraph();
      setAlertText("Node deleted");
      setIsError(false);
      setAlertOpen(true);
    }).catch(error => {
      setAlertText(error + "");
      setIsError(true);
      setAlertOpen(true);
    });
  }

  // Adds a new node to the database, and then reloads the graph.
  function addProblemNode() {
    axios(process.env.REACT_APP_BACKEND_URL + '/api/nodes', {
      method: 'post',
      headers: {
        'Content-Type': 'application/json'
      },
      // TODO tutorials?
      params: {
        topic_id: currTopic.id,
        problem_id: currProblem.id,
        adminPass: global.adminPassword
      },
    }).then((response) => {
      // alert("Node added. Reloading graph.");
      loadGraph();
      setAlertText("Node added");
      setIsError(false);
      setAlertOpen(true);
    }).catch(error => {
      setAlertText(error + "");
      setIsError(true);
      setAlertOpen(true);
    });
  }

  function addTutorialNode() {
    axios(process.env.REACT_APP_BACKEND_URL + '/api/nodes', {
      method: 'post',
      headers: {
        'Content-Type': 'application/json'
      },
      // TODO tutorials?
      params: {
        topic_id: currTopic.id,
        tutorial_id: currTutorial.id,
        adminPass: global.adminPassword
      },
    }).then((response) => {
      // alert("Node added. Reloading graph.");
      loadGraph();
      setAlertText("Node added");
      setIsError(false);
      setAlertOpen(true);
    }).catch(error => {
      setAlertText(error + "");
      setIsError(true);
      setAlertOpen(true);
    });
  }

  //Creates a directed edge from the node with id=fromNode to the node with id=toNode
  function createEdge(fromNode, toNode) {
    axios(process.env.REACT_APP_BACKEND_URL + '/api/edges', {
      method: 'post',
      headers: {
        'Content-Type': 'application/json'
      },
      params: {
        from_node_id: fromNode,
        to_node_id: toNode,
        adminPass: global.adminPassword
      }
    })
      .then((response) => {
        loadGraph();
        setAlertText("Edge added");
        setIsError(false);
        setAlertOpen(true);
        return true;
      }).catch(error => {
        setAlertText(error + "");
        setIsError(true);
        setAlertOpen(true);
        return false;
      });
  }

  //Deletes a directed edge from the node with id=fromNode to the node with id=toNode
  function deleteEdge(fromNode, toNode) {
    console.log("deleteEdge(" + fromNode + ", " + toNode + ");");
    axios(process.env.REACT_APP_BACKEND_URL + '/api/edges', {
      method: 'get',
      headers: {
        'Content-Type': 'application/json'
      },
      params: {
        from_node_id: fromNode,
        to_node_id: toNode
      }
    }).then((response) => {
      let edgeId = response.data[0].id;

      if (edgeId == undefined) {
        alert("Could not find the ID of the edge you're trying to delete.");
        return;
      }

      axios(process.env.REACT_APP_BACKEND_URL + '/api/edges', {
        method: 'delete',
        headers: {
          'Content-Type': 'application/json'
        },
        params: {
          id: edgeId,
          adminPass: global.adminPassword
        }
      }).then((response2) => {
        loadGraph();
        setAlertText("Edge deleted");
        setIsError(false);
        setAlertOpen(true);
      }).catch(error => {
        setAlertText(error + "");
        setIsError(true);
        setAlertOpen(true);
      });

    });

  }

  function erase(){
    //deleteNode(1);
  }

  useEffect(() => getTopics(), []);
  useEffect(() => getProblems(), []);
  useEffect(() => getTutorials(), [currTutorial]);
  useEffect(() => loadGraph(), []);
  useEffect(() => getUsers(), []);
  useEffect(() => props.setNavbarVisible(false), []);


  const handleChange = (event, newValue) => {
    console.log(newValue);
    setSelectedTab(newValue);
  };

  const [selectedTab, setSelectedTab] = useState(0);
  const classes = useStyles();
  return (
    <div>
      <div>
        <div className={classes.root}>
          <Snackbar open={alertOpen} autoHideDuration={3000} onClose={() => setAlertOpen(false)}>
            <Alert elevation={6} variant="filled" severity={isError ? 'error' : 'success'}> {alertText}</Alert>
          </Snackbar>
          <AppBar position="static">
            <Tabs value={selectedTab} onChange={handleChange} >
              {
                topicList.map((item, index) => (
                  <Tab style={{ width: '2vw' }} key={item.title} label={item.title} onClick={() => setCurrTopic(item)} />
                ))}
              <Fab style={{ margin: '15px', width: '50px', height: '50px' }} color="primary" aria-label="add">
                <AddIcon onClick={() => setCreateTopicModalVisible(true)} />
              </Fab>
            </Tabs>
          </AppBar>
        </div>

        <Dialog maxWidth='xl' onClose={() => setCreateTopicModalVisible(false)} open={createTopicModalVisible}>
          <DialogTitle>Create Topic</DialogTitle>
          <TextField style={{ margin: '15px' }} id="outlined-basic" label="New Topic" onChange={(e) => setTopicName(e.target.value)} />

          <Button style={{ margin: '15px' }} variant="contained" onClick={postTopic} color="secondary">
            Post Topic
        </Button>
        </Dialog>
        <TextField style={{ margin: '15px' }} id="outlined-basic" label="Admin Password" type="password" onChange={(e) => { global.adminPassword = e.target.value }} />
        <div style={{ display: 'flex' }}>
          <br></br>


          <FormControl style={{ margin: '15px', width: '150px' }} className={classes.formControl}>
            <InputLabel >Problems</InputLabel>
            <Select
              value={currProblem}
              onChange={loadProblem}
            >
              {
                problemList.map((item) => (
                  <MenuItem value={item}>{item.title}</MenuItem>
                ))}
            </Select>
          </FormControl>
          <Fab style={{ margin: '15px', width: '50px', height: '50px' }} color="primary" aria-label="add">
            <AddIcon onClick={() => setCreateProblemModalVisible(true)} />
          </Fab>
          <Fab style={{ margin: '15px', width: '50px', height: '50px' }} color="secondary" aria-label="delete">
            <TrashIcon onClick={() => deleteProblem()} />
          </Fab>

          <FormControl style={{ margin: '15px', width: '150px' }} className={classes.formControl}>
            <InputLabel >Tutorials</InputLabel>
            <Select
              value={currTutorial}
              onChange={loadTutorial}
            >
              {
                tutorialList.map((item) => (
                  <MenuItem value={item}>{item.title}</MenuItem>
                ))}
            </Select>
          </FormControl>
          <Fab style={{ margin: '15px', width: '50px', height: '50px' }} color="primary" aria-label="add">
            <AddIcon onClick={() => setCreateTutorialModalVisible(true)} />
          </Fab>
        </div>

        <CreateProblemModal
          createProblemModalVisible={createProblemModalVisible}
          setCreateProblemModalVisible={setCreateProblemModalVisible}
          getProblems={getProblems}
          setIsError={setIsError}
          setAlertOpen={setAlertOpen}
          setAlertText={setAlertText}
          getProblems={getProblems}
          currTopic={currTopic}
        />

        <CreateTutorialModal
          createTutorialModalVisible={createTutorialModalVisible}
          setCreateTutorialModalVisible={setCreateTutorialModalVisible}
          getTutorials={getTutorials}
          setIsError={setIsError}
          setAlertOpen={setAlertOpen}
          setAlertText={setAlertText}
        />
        <br></br>
        <Typography style={{ margin: '15px' }} variant="h5">Problem ID: {currProblem.id}</Typography>
        <TextField style={{ margin: '15px' }} id="outlined-basic" label="Problem Title" value={problemTitle} onChange={(e) => setProblemTitle(e.target.value)} />

        <Button style={{ margin: '15px' }} variant="contained" onClick={() => patchProblemTitle()} color="secondary">
          Patch Problem Title
        </Button>

        <br></br>
        <TextField
          id="outlined-multiline-static"
          label="Problem Description"
          multiline
          rows="10"
          style={{ width: '80vw', margin: '15px' }}
          defaultValue=""
          variant="outlined"
          value={problemDescription}
          onChange={(e) => { setProblemDescription(e.target.value) }}
        />

        <Button style={{ margin: '15px' }} variant="contained" onClick={patchProblemDescription} color="secondary">
          Patch Problem Description
        </Button>

        <br></br>

        <TextField
          id="outlined-multiline-static"
          label="Problem Solution"
          multiline
          rows="10"
          style={{ width: '80vw', margin: '15px' }}
          defaultValue=""
          variant="outlined"
          value={problemSolution}
          onChange={(e) => { setProblemSolution(e.target.value) }}
        />

        <Button style={{ margin: '15px' }} variant="contained" onClick={patchProblemSolution} color="secondary">
          Patch Problem Solution
        </Button>

        <br></br>
        <LanguageDropdown setLang={setLang} lang={lang} />
        <br></br>
        <div className="admin-ide-container">
          <div className="admin-ide-block">
            <div className="admin-ide-topbar">
              <Typography style={{ margin: '15px' }} variant="h5">Template Code</Typography>
              <Button style={{ margin: '15px' }} variant="contained" onClick={patchProblemTemplateCode} color="secondary">
                Patch Template Code
                </Button>
            </div>
            <IDEBlock setCode={filterLanguageSetterTemplate()} code={filterLanguageTemplate()} lang={lang} />
          </div>
          <div className="admin-ide-block">
            <div className="admin-ide-topbar">
              <Typography style={{ margin: '15px' }} variant="h5">Test Case Code</Typography>
              <Button style={{ margin: '15px' }} variant="contained" onClick={patchProblemTestCode} color="secondary">
                Patch Test Code
                </Button>
            </div>
            <IDEBlock setCode={filterLanguageSetterTest()} code={filterLanguageTest()} lang={lang} />
          </div>
          <br></br>
        </div>
        <Typography style={{ margin: '15px' }} variant="h5">Data File</Typography>
        <TextField
          id="outlined-multiline-static"
          label="Data File"
          multiline
          rows="4"
          style={{ width: '80vw', margin: '15px' }}
          defaultValue=""
          variant="outlined"
          value={dataFile}
          onChange={(e) => setDataFile(e.target.value)}
        />
        <Button style={{ margin: '15px' }} variant="contained" onClick={patchDataFile} color="secondary">
          Patch Data File
          </Button>
        <br></br>
        <Typography style={{ margin: '15px' }} variant="h5">Tutorial ID: {currTutorial.id}</Typography>
        <TextField style={{ margin: '15px' }} id="outlined-basic" label="Problem Title" value={tutorialTitle} onChange={(e) => setTutorialTitle(e.target.value)} />



        <Button style={{ margin: '15px' }} variant="contained" onClick={() => patchTutorialTitle()} color="secondary">
          Patch Tutorial Title
        </Button>
        <Button style={{ margin: '15px' }} variant="contained" onClick={patchTutorialText} color="secondary">
          Patch Tutorial Text
        </Button>
        <br></br>
        <div style={{ display: 'flex' }}>
          <TextField
            id="outlined-multiline-static"
            label="Tutorial Text"
            multiline
            rows="10"
            style={{ width: '50vw', height: '900px !important', margin: '15px' }}
            defaultValue=""
            variant="outlined"
            value={tutorialText}
            onChange={(e) => { setTutorialText(e.target.value) }}
          />
          <div style={{ width: '50vw' }}>
            <ReactMarkdown style={{ overflowY: 'scroll !important' }} source={tutorialText} escapeHtml={false} renderers={{ code: CodeBlock }} />
          </div>
        </div>




        <br></br>
        <hr></hr>
        <Typography style={{ margin: '15px' }} variant="h5">Graph Editor</Typography>

        {/*
              <Button style={{ margin: '15px' }} variant="contained" onClick={loadExampleGraph} color="secondary">
                Load Example Graph
              </Button>
              */}
        <div className="graph-container">
          <Sigma
            renderer="canvas"
            // graph={nodeGraph}
            settings={{
              drawEdges: true,
              clone: false,
              defaultEdgeColor: 'purple',
              edgeColor: 'default',
              defaultNodeColor: '#ffffff',
              defaultLabelColor: 'white',
              labelSize: 'proportional',
              labelThreshold: 0,
              labelColor: 'red',
              minEdgeSize: 10,
              maxEdgeSize: 10,
              minNodeSize: 15,
              maxNodeSize: 15,
              enableEdgeHovering: true,
            }}
            style={{ height: '80vh', width: '60vw', backgroundColor: 'grey', margin: '15px', boxShadow: '0 1px 3px rgba(0,0,0,0.12), 0 1px 2px rgba(0,0,0,0.24)' }}
            onClickNode={(e) => {
              const { node } = e.data;
              const nodeId = Number(node.id.substr(1));

              node.color = '#00ffff';

              const nodes = e.data.renderer.graph.nodes();
              const fromNode = nodes.find((n) => n.selected === true);

              if (fromNode != undefined) {

                if (!createEdge(Number(fromNode.id.substr(1)), nodeId)) {
                  fromNode.selected = false;
                  console.log("uh oh");
                }

              } else {
                node.selected = true;
              }
            }}
            onClickEdge={(e) => {
              const { edge } = e.data;

              // console.log("test");

              const fromNode = Number(edge.source.substr(1));
              const toNode = Number(edge.target.substr(1));

              deleteEdge(fromNode, toNode);

            }}
          >
            <NodeShapes default="circle" />
            <EdgeShapes default="line" />
            <RelativeSize initialSize={15} />

            <RefreshableSigma currGraph={nodeGraph}>
              <RelativeSize initialSize={15} />
            </RefreshableSigma>
          </Sigma>
        </div>
        <Typography style={{ margin: '15px' }} variant="h5">Add Nodes</Typography>
        <div className="row">
          <Button style={{ margin: '15px' }} variant="contained" onClick={addProblemNode} color="secondary">
            Add Problem Node
          </Button>
          <Button style={{ margin: '15px' }} variant="contained" onClick={addTutorialNode} color="secondary">
            Add Tutorial Node
          </Button>

          <Button style={{ margin: '15px' }} variant="contained" onClick={()=> erase()} color="secondary">
            dont touch
          </Button>
        </div>
      </div>
    </div>
  );
}

/*
  react-sigma doesn't refresh the graph when you update its state (i.e. the graph property),
  so unfortunately, we need a child component that will manage manual refreshes of sigma.
*/
class RefreshableSigma extends React.Component {

  render() {
    return <div>{embedProps(this.props.children, { sigma: this.props.sigma })}</div>
  }

  componentDidUpdate(props) {

    let sigmaGraph = this.props.sigma.graph;
    let newGraph = this.props.currGraph;

    //Don't refresh the graph unless new nodes or edges have been added, or an edge was removed.
    if (props.currGraph !== '' &&
      newGraph.nodes.length === props.currGraph.nodes.length &&
      newGraph.edges.length === props.currGraph.edges.length) {
      return;
    }

    if (newGraph.nodes != undefined) {
      //Clear the current graph
      sigmaGraph.clear();

      //Add all the nodes for the new graph
      newGraph.nodes.forEach(function (node) {
        // node.x = Math.floor(Math.random() * (200));
        // node.y = Math.floor(Math.random() * (200));
        sigmaGraph.addNode(node);
      });

      //Add all the edges for the new graph
      newGraph.edges.forEach(function (edge) {
        sigmaGraph.addEdge(edge);
      });

      // console.log(newGraph);

      //Redraw the sigma graph (required, because react-sigma doesn't update when its state changes)
      this.props.sigma.refresh();
    }
  }
}
