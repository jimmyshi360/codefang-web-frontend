import React, { useEffect } from 'react';

import AOS from 'aos';
import StickyFooters from './home/stickyFooters';

import TeamMember from './team/teammember';
import Jimmy from '../imgs/team/jimmy.png';
import Andrew from '../imgs/team/az.png';
import Brice from '../imgs/team/brice.png';
import Dan from '../imgs/team/dan.png';
import Steven from '../imgs/team/steven.jpg';
import Alex from '../imgs/team/alex.jpg';

import Palantir from '../imgs/companies/palantir.png';
import Bloomberg from '../imgs/companies/bloomberg.png';
import PayPal from '../imgs/companies/paypal.png';
import Amazon from '../imgs/companies/amazon.png';
import Google from '../imgs/companies/google.png';
import Barclays from '../imgs/companies/barclays.png';

import 'aos/dist/aos.css';

export default function Team(props) {
  const members = [{
    headshotURL: Jimmy,
    name: 'Jimmy Shi',
    position: 'Technical Product Manager',
    companyTitle: 'Software Engineer Intern at ',
    companyPicture: Palantir,
  }, {
    headshotURL: Andrew,
    name: 'Andrew Zhang',
    position: 'Product Manager',
    companyTitle: 'Software Engineer Intern at ',
    companyPicture: Amazon,
  },
  {
    headshotURL: Dan,
    name: 'Daniel Qian',
    position: 'Backend',
    companyTitle: 'Former Software Engineer Intern at ',
    companyPicture: Bloomberg,
  },
  {
    headshotURL: Brice,
    name: 'Brice Halder',
    position: 'Backend',
    companyTitle: 'Former Software Engineer Intern at ',
    companyPicture: PayPal,
  },
  {
    headshotURL: Steven,
    name: 'Steven Solar',
    position: 'Frontend',
    companyTitle: 'Software Engineer Intern at ',
    companyPicture: Google,
  },
  {
    headshotURL: Alex,
    name: 'Alexander Atschinow',
    position: 'Frontend',
    companyTitle: 'Former Software Engineer Intern at ',
    companyPicture: Barclays,
  },
  ];
  AOS.init({ duration: 2000, once: true });
  let delay = 500;
  useEffect(() => props.setNavbarVisible(true), []);
  
  return (
    <div className="team-container">
      <div
        className="header-container"
        data-aos="fade-up"
        data-aos-duration="1000"
        data-aos-delay="0"
      >
        <h1>We help coders on their way to landing their dream jobs.</h1>
        <div className="hr-theme-slash-2">
          <div className="hr-line" />
        </div>
        <br />
      </div>
      <h2
        data-aos="fade-up"
        data-aos-duration="1000"
        data-aos-delay="100"
      >
        Meet the Team
      </h2>
      <p
        className="descText"
        data-aos="fade-up"
        data-aos-duration="1000"
        data-aos-delay="200"
      >
        We&apos;ve been there. Too often, software engineers are faced
        with the daunting spectacle of a technical interview, often not having learned concepts due to
        being self-taught or going through university curricula seemingly unrelated to interview
        material. That&apos;s why our team created CodeCanopy, a technical interview training site
        designed by past SWE interns at big tech companies who had few tools at their disposal themselves, and are
        looking to bring a torchlight to the process.
      </p>
      <br />
      <div className="testimonials-container">
        {members.map((item) => (
          <TeamMember
            headshotURL={item.headshotURL}
            name={item.name}
            companyTitle={item.companyTitle}
            companyPicture={item.companyPicture}
            position={item.position}
            delay={delay += 100}
          />
        ))}
      </div>
      <StickyFooters />
    </div>
  );
}
