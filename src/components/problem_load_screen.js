import React, { useState, useEffect } from 'react';
import Particles from 'react-particles-js';
import Logo from '../imgs/logo-center.png';

export default function ProblemLoadScreen(props) {
  const [loadState, setLoadState] = useState(0);
  const loadingText = [ "Fetching WebGL files", "Code Engine starting up", "Loading Atlas Skill Tree", "Readying UI", "Readying UI"];
  const particlesOptions1 = {
    particles: {
      number: {
        value: 12,
        density: {
          enable: true,
          value_area: 789.1476416322727,
        },
      },
      color: {
        value: '#e4ebf0',
      },
      shape: {
        type: 'polygon',
        stroke: {
          width: 0,
          color: '#e4ebf0',
        },
        polygon: {
          nb_sides: 6,
        },
        image: {
          src: 'img/github.svg',
          width: 100,
          height: 150,
        },
      },
      opacity: {
        value: 1,
        random: false,
        anim: {
          enable: true,
          speed: 1,
          opacity_min: 0.8,
          sync: false,
        },
      },
      size: {
        value: 1.5,
        random: true,
        anim: {
          enable: true,
          speed: 2,
          size_min: 0,
          sync: false,
        },
      },
      line_linked: {
        enable: false,
        distance: 150,
        color: '#ffffff',
        opacity: 0.4,
        width: 1,
      },
      move: {
        enable: true,
        speed: 10,
        direction: 'top',
        random: false,
        straight: true,
        out_mode: 'out',
        bounce: false,
        attract: {
          enable: false,
          rotateX: 0,
          rotateY: 0,
        },
      },
    },
    interactivity: {
      detect_on: 'canvas',
      events: {
        onhover: {
          enable: false,
          mode: 'repulse',
        },
        onclick: {
          enable: false,
          mode: 'push',
        },
        resize: true,
      },
      modes: {
        grab: {
          distance: 200,
          line_linked: {
            opacity: 1,
          },
        },
        bubble: {
          distance: 200,
          size: 40,
          duration: 2,
          opacity: 8,
          speed: 3,
        },
        repulse: {
          distance: 100,
        },
        push: {
          particles_nb: 4,
        },
        remove: {
          particles_nb: 2,
        },
      },
    },
    retina_detect: true,
  };

  const particlesOptions2 = {
    particles: {
      number: {
        value: 12,
        density: {
          enable: true,
          value_area: 789.1476416322727,
        },
      },
      color: {
        value: '#d8dae3',
      },
      shape: {
        type: 'polygon',
        stroke: {
          width: 0,
          color: '#d8dae3',
        },
        polygon: {
          nb_sides: 4,
        },
        image: {
          src: 'img/github.svg',
          width: 100,
          height: 150,
        },
      },
      opacity: {
        value: 1,
        random: false,
        anim: {
          enable: true,
          speed: 1,
          opacity_min: 0.8,
          sync: false,
        },
      },
      size: {
        value: 2,
        random: true,
        anim: {
          enable: true,
          speed: 2,
          size_min: 0,
          sync: false,
        },
      },
      line_linked: {
        enable: false,
        distance: 50,
        color: '#253371',
        opacity: 1,
        width: 2,
      },
      move: {
        enable: true,
        speed: 3,
        direction: 'top',
        random: false,
        straight: true,
        out_mode: 'out',
        bounce: false,
        attract: {
          enable: false,
          rotateX: 0,
          rotateY: 0,
        },
      },
    },
    interactivity: {
      detect_on: 'canvas',
      events: {
        onhover: {
          enable: false,
          mode: 'repulse',
        },
        onclick: {
          enable: false,
          mode: 'push',
        },
        resize: true,
      },
      modes: {
        grab: {
          distance: 200,
          line_linked: {
            opacity: 1,
          },
        },
        bubble: {
          distance: 200,
          size: 40,
          duration: 2,
          opacity: 8,
          speed: 3,
        },
        repulse: {
          distance: 100,
        },
        push: {
          particles_nb: 4,
        },
        remove: {
          particles_nb: 2,
        },
      },
    },
    retina_detect: true,
  };
  useEffect(() => {
        setLoadState(parseInt(props.progress*100/25+""));
  }, [props.progress]);

  return (
    <div className="halo-cover">

      {/*<div className="spinner"></div>*/}
      <img src={Logo} style={{margin: 'auto', width: '120px' , marginTop: '40vh' }}/>
      <div className="halo-words-2">
      {
          loadingText[loadState].split('').map((item) => (
            <div className="halo">
              <span className="halo-anim">
                {item}
              </span>



            </div>))

        }
            </div> 

      <progress style={{ margin: 'auto', height: '6px', width: '400px'}} id="file" value={props.progress*100} max="100"> {props.progress*100}% </progress>
     
            
      <div className="halo-words-2">
    <div className="halo">
              <span className="halo-anim">
              &nbsp; {(props.progress*6.2).toFixed(2)} MB/6.2 MB
                </span>
            </div>
      </div>
      {/*<Particles className="particles" params={particlesOptions1} />*/}
      {/*<Particles className="particles" params={particlesOptions2} />*/}
    </div>
  );
}
