import React, { useState, useEffect } from 'react';
import axios from 'axios';
import StickyFooters from './home/stickyFooters';

export default function Profile(props) {
  /** can you update these three states when the user is loggedin/logged out? */

  return (
    <div className="profile-container">
      <div className="profile-card">
        <img src="" className="profile-pic"/>
        <p>{props.name}</p>
        <p>{props.email}</p>
        <p>Level: 5</p>
        <div className="skillbar-container">
          <div className="skillbar"></div>
        </div>
      </div>
      <StickyFooters />
    </div>
  );
}
