import React, { useState } from 'react';
import '../css/Home.css';
import '../css/Login.css';
import GoogleButton from 'react-google-button';
import { Link } from 'react-router-dom';
import StickyFooters from './home/stickyFooters';

export default function Signup(props) {
  /** can you update these three states when the user is loggedin/logged out? */
  const [isLoggedIn, setLoggedInStatus] = useState(false);
  const [name, setName] = useState('');
  const [email, setEmail] = useState('');

  return (
    <div className="login-background">
      <h1 className="login-welcome">
        Welcome
      </h1>
      <div className="login-box">
        <img alt="CodeFang" src="" />
        <br />
        <form
          onsubmit={() => setLoggedInStatus(true)}
          action="/api/auth/local/create"
          method="post">
          <div>
              <input
                className="login-text-box"
                Placeholder="Name"
                type="text"
                name="name"
                onChange={(event) => {
                  setName(event.target.value);
                  console.log(event);
                }}/>
          </div>
          <div>
              <input
                className="login-text-box"
                placeholder="Email"
                type="text"
                name="email"
                onChange={(event) => {
                  setEmail(event.target.value);
                  console.log(event);
                }}/>/>
          </div>
          <div>
              <input
                className="login-text-box"
                placeholder="Password"
                type="password"
                name="password"/>
          </div>
          <div>
              <input
                className="login-text-box"
                type="submit"
                value="Create User"/>
          </div>
        </form>
        <Link to="login">Already have an account? Sign in here.</Link>

      </div>
      <br />
      <br />
      <StickyFooters />
    </div>

  );
}

/*
 *
 <div>
            <GoogleLogin
              clientId="658977310896-knrl3gka66fldh83dao2rhgbblmd4un9.apps.googleusercontent.com"
              buttonText="Login with Google"
              onSuccess={responseGoogle}
              onFailure={responseGoogle}
              cookiePolicy="single_host_origin"
            />
          </div>
  */
