import React, { useState, useEffect } from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faAtlas, faCodeBranch, faCode, faList, faExpandAlt, faCompress, faUsers, faSignOutAlt, faUser, faCog, faSignInAlt } from '@fortawesome/free-solid-svg-icons'
import ReactSearchBox from 'react-search-box';
import Logo from '../imgs/logowhite.png';
import Autosuggest from 'react-autosuggest';
import theme from '../css/SearchBar.css';
import Toggle from 'react-toggle';
import "react-toggle/style.css"
import ProfilePic from '../imgs/profile.png';

export default function LeftBar(props) {

    function openSettings() {
        document.getElementById("mySettings").style.display = "block";
    }

    function closeSettings() {
        document.getElementById("mySettings").style.display = "none";
    }

    function openProfile() {
        document.getElementById("myProfile").style.display = "block";
    }

    function closeProfile() {
        document.getElementById("myProfile").style.display = "none";
    }

    function openPleaseLogIn() {
        document.getElementById("myPleaseLogIn").style.display = "block";
    }

    function closePleaseLogIn() {
        document.getElementById("myPleaseLogIn").style.display = "none";
    }
  /** 
  function blobPanel(){
    var height = window.innerHeight,
  x= 0, y= height/2,
    curveX = 10,
    curveY = 0,
    targetX = 0,
    xitteration = 0,
    yitteration = 0,
    menuExpanded = false;
    
    blob = document.getElementById('blob');
    blobPath = document.getElementById('blob-path');

    hamburger = $('.hamburger');

    $(this).on('mousemove', function(e){
        x = e.pageX;
        
        y = e.pageY;
    });




    $('.hamburger, .menu-inner').on('mouseenter', function(){
        $(this).parent().addClass('expanded');
        menuExpanded = true;
    });

    $('.menu-inner').on('mouseleave', function(){
        menuExpanded = false;
        $(this).parent().removeClass('expanded');
    });

    function easeOutExpo(currentIteration, startValue, changeInValue, totalIterations) {
        return changeInValue * (-Math.pow(2, -10 * currentIteration / totalIterations) + 1) + startValue;
    }

    var hoverZone = 150;
    var expandAmount = 20;
    
    function svgCurve() {
        if ((curveX > x-1) && (curveX < x+1)) {
            xitteration = 0;
        } else {
            if (menuExpanded) {
                targetX = 0;
            } else {
                xitteration = 0;
                if (x > hoverZone) {
                    targetX = 0;
                } else {
                    targetX = -(((60+expandAmount)/100)*(x-hoverZone));
                }           
            }
            xitteration++;
        }

        if ((curveY > y-1) && (curveY < y+1)) {
            yitteration = 0;
        } else {
            yitteration = 0;
            yitteration++;  
        }

        curveX = easeOutExpo(xitteration, curveX, targetX-curveX, 100);
        curveY = easeOutExpo(yitteration, curveY, y-curveY, 100);

        var anchorDistance = 200;
        var curviness = anchorDistance - 40;

        var newCurve2 = "M60,"+height+"H0V0h60v"+(curveY-anchorDistance)+"c0,"+curviness+","+curveX+","+curviness+","+curveX+","+anchorDistance+"S60,"+(curveY)+",60,"+(curveY+(anchorDistance*2))+"V"+height+"z";

        blobPath.attr('d', newCurve2);

        blob.width(curveX+60);

        hamburger.css('transform', 'translate('+curveX+'px, '+curveY+'px)');
    
    window.requestAnimationFrame(svgCurve);
    
  }

  useEffect(()=>{blobPanel()}, []);

  */

  return (
    <>
    <nav className={props.problemIndex==2 ? "left-navbar-blur blurred-bar" : "left-navbar-blur"} style={{ left: '0', top: '40px', width: '80px', height: window.innerHeight-55+'px', zIndex: '80' }}></nav>
    <nav className="left-navbar" style={{ left: '0', top: '55px', width: '50px', height: window.innerHeight-85+'px', zIndex: '90' }}>
     
        <div>
          <ul className="spec2" style={{ marginTop: '0px', paddingLeft: '0', display: 'flex' }}>

            {/*<li className={props.problemIndex == 0 ? "accordian-li selected-accordian" : "accordian-li"}>
              <a className={props.problemIndex == 0 ? "glowing condensed-link" : 'condensed-link'} onClick={() => props.setProblemIndex(0)}>
                <FontAwesomeIcon icon={faAtlas} className={props.problemIndex == 0 ? "glowing condensed-icon" : "condensed-icon"} /> Encyclopedia
                
              </a>
            </li>*/}
            
            <li className="accordian-li" >
              <a className={props.problemIndex == 2 ? "glowing condensed-link condensed-link-active" : 'condensed-link'} onClick={() => {if(props.problemIndex!=2) props.setProblemIndex(2)
              else props.setProblemIndex(-1)}}>
                <FontAwesomeIcon icon={faCode} className={props.problemIndex == 2 ? "glowing condensed-icon" : "condensed-icon"} />
              </a>
                {props.isLoggedIn ?
                (<a className='condensed-link' onClick={openProfile}>
                    <FontAwesomeIcon icon={faUser} className='condensed-icon' />
                </a>) :
                (<a className='condensed-link' onClick={openPleaseLogIn}>
                    <FontAwesomeIcon icon={faUser} className='condensed-icon' />
                </a>)
                }
            </li>

            <div style={{position: 'absolute', bottom: '50px', width: '50px' }}>
            <li className="accordian-li" >
              <a className='condensed-link' onClick={openSettings}>
                <FontAwesomeIcon icon={faCog} className='condensed-icon' />
              </a>

              {props.isLoggedIn ? 
              (<a className='condensed-link'  href="/api/auth/logout">
                <FontAwesomeIcon icon={faSignOutAlt} className='condensed-icon' />
              </a>) :

              (<a className='condensed-link'  href="/api/auth/logout">
                <FontAwesomeIcon icon={faSignInAlt} className='condensed-icon' />
              </a>)
              }
            </li>
            </div>

          </ul>
        </div>

        <div className="settings-popup" id="mySettings">
            <form action="/action_page.php" className="settings-container">
                <h1>Settings</h1>
                <h2>Font Size</h2>
                <h2>Tab Size</h2>
                <button type="button" className="settings-close"
                        onClick={closeSettings}>Close
                </button>
            </form>
        </div>
        <div className="profile-popup" id="myProfile">
            <form action="/action_page.php" className="profile-container">
                <h1>Name:</h1>
                <button type="button" className="profile-close"
                        onClick={closeProfile}>Close
                </button>
            </form>
        </div>
        <div className="pleaseLog-popup" id="myPleaseLogIn">
            <form action="/action_page.php" className="pleaseLog-container">
                <h1>Please Log In</h1>
                <button type="button" className="pleaseLog-close"
                        onClick={closePleaseLogIn}>Close
                </button>
            </form>
        </div>
                  
    </nav>
    </>
  );
}
